/**
 * 
 */
package app;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import ob.util.Utils;
import processing.GeneralUtils;

import blog.threaded.Comment;
import blog.threaded.livejournal.Blog;
import blog.threaded.livejournal.Entry;

import utils.*;
import weka.core.Instances;
import modules.*;

/**
 * @author sara
 *
 */
public class Influencer {
	
	public static boolean DEBUG = false;
	boolean _overwrite = false;

	ClaimModule _claimDetector = null;
	ArgumentationModule _argumentationDetector = null;
	AgreementModule _agreementDetector = null;
	PersuasionModule _persuasionDetector = null;
	DialogModule _dialogModule = null;
	DemographicModule _demographicModule = null;
	CredibilityModule _credibilityModule = null;
	WekaBuilder _weka = null;
	MaxentTagger _tagger = null;
	LexicalizedParser _parser = null;
			
	String _processingDirectory = "";
	
	/**
	 * 
	 */
	public Influencer(ArrayList<String> domains, boolean dialog, boolean agreement, boolean claim, boolean argumentation, 
			boolean persuasion, boolean demographics, boolean credibility, boolean topics, boolean exact, boolean overwrite, boolean processing, int part, int numparts) {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			_processingDirectory = prop.getProperty("processing");
			_tagger = new MaxentTagger(prop.getProperty("tagger"));
			_parser = LexicalizedParser.loadModel();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (agreement) _agreementDetector = new AgreementModule();
		if (claim) _claimDetector = new ClaimModule(processing);
		if (argumentation) _argumentationDetector = new ArgumentationModule();
		if (persuasion) _persuasionDetector = new PersuasionModule(false,_claimDetector,_argumentationDetector,processing);
		if (dialog) _dialogModule = new DialogModule();
		if (demographics) _demographicModule = new DemographicModule(_tagger, _parser, processing);
		if (credibility) _credibilityModule = new CredibilityModule();
		
		_overwrite = overwrite;
		_weka = new WekaBuilder(domains, claim, argumentation, persuasion, agreement, dialog, demographics, credibility, topics, exact, overwrite, part, numparts);
	}
	
	public void run(HashMap<String,String> trainFiles, ArrayList<HashMap<String,String>> testFiles, String classifier, 
			HashMap<String,HashMap<String,Annotation>> trainAnnotations, 
			ArrayList<HashMap<String,HashMap<String,Annotation>>> testAnnotations, String outputDir, String experimentList,
			boolean exclude, boolean domainAdaptation, HashMap<String,String []> topics, 
			ArrayList<String> trainnames, ArrayList<String> testnames) {
		
		System.err.println("Load Training ... ");
		
		Instances training = null;
		if (_overwrite || !new File(outputDir + "/influence-train.arff").exists()) {
			//if (exclude) {
				trainFiles = excludeNoInfluencers(trainFiles, trainAnnotations);
			//}
			training = loadInstances(trainFiles,trainAnnotations, topics, "training", true);
		}
		else {
			training = _weka.arffLoader(outputDir + "/influence-train.arff");
		}
		System.err.println("Load Testing ... ");
		
		HashMap<String,Instances> testing = new HashMap<String,Instances>();
		
		for (int i = 0; i < testFiles.size(); i++) {
			testing.put(testnames.get(i) + "-include--",loadInstances(testFiles.get(i), testAnnotations.get(i), topics, "testing", false));
		}

		if (exclude) {
			System.err.println("Exclude files without influencers...");
			ArrayList<HashMap<String,String>> testFilesExclude = excludeNoInfluencers(testFiles, testAnnotations);
			
			for (int i = 0; i < testFilesExclude.size(); i++) {
				testing.put(testnames.get(i) + "-exclude--",loadInstances(testFilesExclude.get(i), testAnnotations.get(i), topics, "testing", false));
			}
		}
		
		HashMap<String, double [][]> classifications = new HashMap<String,double [][]>();
		
		HashMap<String,ArrayList<Double>> predictions = 
				_weka.classify(classifier, classifications, training, testing, outputDir, experimentList, domainAdaptation, true, trainnames, testnames);
		
		List<String> best = printResults(outputDir,predictions, testing, testnames);
		printStatSignificance(outputDir, testing, predictions, testnames, best);
		
	}
		
	private ArrayList<HashMap<String,String>> excludeNoInfluencers(ArrayList<HashMap<String,String>> files, ArrayList<HashMap<String,HashMap<String,Annotation>>> annotations) {
		
		ArrayList<HashMap<String,String>> allinclude = new ArrayList<HashMap<String,String>>();
		
		for (int i = 0; i < files.size(); i++) {
			allinclude.add(excludeNoInfluencers(files.get(i),annotations.get(i)));
		}
		return allinclude;
	}
	
	private HashMap<String,String> excludeNoInfluencers(HashMap<String,String> files, HashMap<String,HashMap<String,Annotation>> annotations) {
		
			HashMap<String,String> include = new HashMap<String,String>();
			
			int count = 0;
			
			for (String file : files.keySet()) {
				if (annotations.containsKey(file) && (annotations.get(file).size() > 1 || !annotations.get(file).containsKey("none"))) {
					include.put(file,files.get(file));
					count += annotations.get(file).size();
				}
				else {
					System.out.println("No annotations in " + file);
					//System.out.println(annotations.get(file).toString() + "\n");
				}
			}
			System.out.println("There are " + count + " relevant annotations.");
		return include;
	}
	
	public void statistics(HashMap<String,String> fileList, HashMap<String,HashMap<String,Annotation>> annotations, HashMap<String,String []> topics, boolean binned) {
		/*String [] files = new File(directory + "/threads/").list();
		
		HashMap<String,String> fileList = new HashMap<String,String>();
		for (String file : files) {
			fileList.put(file,"");
		}
		
		System.out.println(files.length + " FILES!");*/
		//Instances instances = 
		loadInstances(fileList, annotations, topics, "stats", true);
		//_weka.computeCorrelation(instances,binned);
	}
	
	public ArrayList<String> loadFilesFromAnnotations(String directory, Set<String> fileNames) {
		
		ArrayList<String> files = new ArrayList<String>();
		
		for (String file : fileNames) {
			if (new File(directory + "/" + file).exists()) files.add(file);
			else System.err.println("Missing File: " + file);
		}
		return files;
	}
	
	/*public void crossvalidation(String directory, HashMap<String,HashMap<String,Annotation>> annotations, 
			HashMap<String,String> fileList, String classifier, int folds, String outputDir,
			String experimentList, boolean domainAdaptation, HashMap<String,String []> topics,
			ArrayList<String> domainnames) {
		
		System.out.println(fileList.size() + " files. ");
		
		//int nTrain = files.length - (files.length/folds);
		int nTest = fileList.size()/folds;
		
		HashMap<String,double []> results = new HashMap<String, double []>();
		
		for (int n = 0; n < folds; n++) {
			System.out.println("Train: 0-" + n*nTest + ", " + ((n * nTest) + nTest + 1) + "-" + fileList.size() + ". Test: " + (n * nTest) + "-" + ((n * nTest) + nTest + 1));
			
			String [] files = (String [])fileList.keySet().toArray();
			String [] test = Arrays.copyOfRange(files, n * nTest, (n * nTest) + nTest + 1);
			String [] train = (String [])ArrayUtils.addAll(Arrays.copyOfRange(files, 0, n * nTest), Arrays.copyOfRange(files, (n * nTest) + nTest +1, files.length));			

			HashMap<String,String> trainFiles = new HashMap<String,String>();
			for (String file : train)
				trainFiles.put(file, fileList.get(file));
			trainFiles = excludeNoInfluencers(trainFiles, annotations);
			
			HashMap<String,String> testFiles = new HashMap<String,String>();
			for (String file : test)
				testFiles.put(file, fileList.get(file));
			testFiles = excludeNoInfluencers(testFiles, annotations);
			
			Instances training = loadInstances(trainFiles, annotations, topics, "training", true);
			Instances testing = loadInstances(testFiles, annotations, topics, "testing", false);
			
			System.out.println("Train: " + training.numInstances() + ", Test: " + testing.numInstances());
			
			HashMap<String, double [][]> classifications = new HashMap<String,double [][]>();
			ArrayList<Instances> testwrapper = new ArrayList<Instances>();
			testwrapper.add(testing);
			
			_weka.classify(classifier, classifications, training, testwrapper, outputDir, experimentList, domainAdaptation, true, domainnames, domainnames);
					
			for (String experimentName : classifications.keySet()) {
				double TP = classifications.get(experimentName)[1][1];
				double TN = classifications.get(experimentName)[0][0];
				double FN = classifications.get(experimentName)[1][0];
				double FP = classifications.get(experimentName)[0][1];
				double P = TP+FP == 0 ? 0 : (TP/(TP+FP));
				double R = TP+FN == 0 ? 0 : (TP/(TP+FN));
				
				double correct = TP + TN;
				double incorrect = FP + FN;
				double total = correct + incorrect;
				
				double [] r = results.containsKey(experimentName) ? results.get(experimentName) : new double [4];
				r[0] += correct/total;
				r[1] += P;
				r[2] += R;
				r[3] += P + R == 0 ? 0 : 2 * (P*R)/(P+R);
				results.put(experimentName, r);
			}							
		}
		for (String experiment : results.keySet()) {
			double [] result = results.get(experiment);
			System.out.println(experiment + "," + result[0]/folds + "," + result[1]/folds + "," + result[2]/folds + "," + result[3]/folds);
		}
	}*/
	
	private void printStatSignificance(String outputDir, HashMap<String,Instances> gold, HashMap<String,ArrayList<Double>> predictions, 
			ArrayList<String> testnames, List<String> best) {
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(outputDir + "/significance.txt",true)); 
			BufferedWriter results = new BufferedWriter(new FileWriter(outputDir + "/predictions.txt",true));
			
			for (String key : gold.keySet()) {
				results.write("0gold---_" + key); 
				for (int index = 0; index < gold.get(key).numInstances(); index++) {
						results.write("," + gold.get(key).instance(index).value(gold.get(key).attribute("influencer")));
				}
				results.write("\n");
			}
			
			for (String experiment : predictions.keySet()) {
					
					//boolean isBest = isBest(best,experiment);
					//out.write((isBest ? "best---" : "") + experiment); 
					//results.write((isBest ? "best---" : "") + experiment);
					out.write(experiment); 
					results.write(experiment);
					
					for (int index = 0; index < predictions.get(experiment).size(); index++) {
						Instances g = gold.get(experiment.substring(0,experiment.indexOf("--")+2)); //getDomainNameIndex(experiment,testnames));
						//out.write("," + ((NominalPrediction)predictions.get(experiment).elementAt(index)).predicted());
						results.write("," + predictions.get(experiment).get(index));
						out.write("," + (g.instance(index).value(g.attribute("influencer")) == 
								(double)predictions.get(experiment).get(index) ? 1 : 0));
					}
					out.write("\n");
					results.write("\n");
				}
				out.close();
				results.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/*private boolean isBest(List<String> best, String experimentName) {
		for (String b : best) {
			if (b.endsWith(experimentName)) return true;
		}
		return false;
	}*/
	
	/*private String getDomainNameIndex(String experiment, ArrayList<String> domains) {
		for (int i = 0; i < domains.size(); i++) {
			if (experiment.indexOf(domains.get(i)) >= 0)
				return domains.get(i);
		}
		return null;
	}*/
	
	/**
	 * Print all results to output file and return best result
	 * @param outputDir
	 * @param classifications
	 * @param gold
	 * @param testnames
	 * @return
	 */
	private List<String> printResults(String outputDir, HashMap<String,ArrayList<Double>> classifications, HashMap<String,Instances> gold, ArrayList<String> testnames) {

		List<String> best = new ArrayList<String>();
		//double [] max = new double[testnames.size()];
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(outputDir + "/results.txt", true));

			for (String experimentName : classifications.keySet()) {
				String domainIndex = experimentName.substring(0,experimentName.indexOf("--")+2); //getDomainNameIndex(experimentName,testnames);
				Instances g = gold.get(domainIndex);
					
				ArrayList<Double> predictions = classifications.get(experimentName);	
				double [] values = computeFScore(predictions, g);
				double TP = values[0], TN = values[1], FP = values[2], FN = values[3]; 
				
				double P = TP+FP == 0 ? 0 : (TP/(TP+FP));
				double R = TP+FN == 0 ? 0 : (TP/(TP+FN));
				double F = (P == 0 ? 0 : 2 * (P*R)/(P+R));
				
				/*if (F >= max[domainIndex]) {
					// in ties, keep one with most features
					if (F == max[domainIndex] && experimentName.length() < best.get(domainIndex).length()) {
						if (best.indexOf("ranking") >= 0 && 
								best.get(domainIndex).substring(0,best.get(domainIndex).indexOf("_ranking")).equals(experimentName)) {
							best.set(domainIndex, experimentName);
							max[domainIndex] = F;
						}
					}
					// if tie is ranking vs non-ranking, keep non-ranking
					else if (experimentName.indexOf("ranking") >= 0 && 
							experimentName.substring(0,experimentName.indexOf("_ranking")).equals(best) && F == max[domainIndex]) {
					}
					else {
						max[domainIndex] = F;
						best.set(domainIndex, experimentName);
					}
				}*/
				
				double correct = TP + TN;
				double incorrect = FP + FN;
				double total = correct + incorrect;
					
				out.write(experimentName + "," + TP + "," + TN + "," + FP + "," + FN + "," + 
						(total == 0 ? 0 : (correct/total)*100) + "," + P + "," + R + "," + F + "\n");
			}

			/*for (int i = 0; i < best.size(); i++) {
				double [] values = computeFScore(classifications.get(best), gold.get(getDomainNameIndex(best.get(i),testnames)));
				double TP = values[0], TN = values[1], FP = values[2], FN = values[3]; 
				double P = TP+FP == 0 ? 0 : (TP/(TP+FP));
				double R = TP+FN == 0 ? 0 : (TP/(TP+FN));
				double F = (P == 0 ? 0 : 2 * (P*R)/(P+R));
				double correct = TP + TN;
				double incorrect = FP + FN;
				double total = correct + incorrect;
				out.write("best---" + best.get(i) + "," + TP + "," + TN + "," + FP + "," + FN + "," + 
						(total == 0 ? 0 : (correct/total)*100) + "," + P + "," + R + "," + F + "\n");
			}*/
			out.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return best;
	}
	
	private double [] computeFScore(ArrayList<Double> predictions, Instances g) {
		 
		double [] values = {0,0,0,0};
		
		//double TP = 0; //classifications.get(experimentName)[1][1];
		//double TN = 0; //classifications.get(experimentName)[0][0];
		//double FP = 0; //classifications.get(experimentName)[1][0];
		//double FN = 0; //classifications.get(experimentName)[0][1];
		
		for (int i = 0; i < predictions.size(); i++) {
			if (predictions.get(i) == g.instance(i).classValue() && predictions.get(i) == 1) values[0]++; //TP
			else if (predictions.get(i) == g.instance(i).classValue() && predictions.get(i) == 0) values[1]++; //TN
			else if (predictions.get(i) != g.instance(i).classValue() && predictions.get(i) == 1) values[2]++; //FP
			else values[3]++; //FN
		}
		return values;
	}
	
	public HashMap<String, HashMap<String,Annotation>> loadFailedPOPs(String iFile, String pFile, String type) {
		HashMap<String,HashMap<String,Annotation>> influencers = loadAnnotations(iFile,type);
		HashMap<String,HashMap<String,Annotation>> pops = loadAnnotations(pFile,type);
		
		HashMap<String,HashMap<String,Annotation>> failedPoPs = new HashMap<String,HashMap<String,Annotation>>();
		
		for (String file : pops.keySet()) {
			HashMap<String,Annotation> p = pops.get(file);
			
			if (!influencers.containsKey(file)) {
				failedPoPs.put(file,p);
				continue;
			}
			
			HashMap<String,Annotation> a = new HashMap<String,Annotation>();
			for (String author : p.keySet()) {
				if (!influencers.get(file).containsKey(author)) a.put(author, p.get(author));
				// possibly include influencers where confidence is low
				// else if (influencers.get(file).p.get(author))
			}
			failedPoPs.put(file, a);
		}
		return failedPoPs;
	}
	
	private HashMap<String,HashMap<String,Annotation>> loadAnnotations(String filename, String type) {
		
		int count = 0;
		HashMap<String, HashMap<String,Annotation>> annotations = new HashMap<String, HashMap<String,Annotation>>();
		
		List<String> lines = Utils.readLines(filename);
		
		for (String line : lines) {
			String [] annotation = line.split(",");
			
			HashMap<String,Annotation> influencers = annotations.containsKey(annotation[1].trim()) ? annotations.get(annotation[1].trim()) : new HashMap<String,Annotation>();
			Annotation a = new Annotation(annotation[0],annotation[1],annotation[2],annotation[3],annotation[4]);
			
			//if (a.getAnnotator().equals("Ethan") && type.equals("wikipedia")) continue;
			//if (a.getConfidence().equals("Low")) continue;
			
			influencers.put(a.getAuthor().toLowerCase(),a);
			annotations.put(annotation[1].trim(), influencers );
			if (!annotation[2].toLowerCase().trim().equals("none")) count++;
			if (!new File("/proj/fluke/users/sara/influence/processed/" + type + "/processed-" + annotation[1].trim()).exists()) {
				System.err.println("Missing: " + "/proj/fluke/users/sara/influence/processed/" + type + "/processed-" + annotation[1].trim());
			}
		}
		System.out.println(lines.size() + " annotations including none. " + count + " annotations excluding none. " + annotations.size() + " files.");
		return annotations;
	}
	
	private HashMap<String,String []> loadDemographicArea(String filename) {
		HashMap<String,String []> topics = new HashMap<String, String []>(); 
		
		List<String> lines = Utils.readLines(filename);
		
		for (String line : lines) {
			String [] annotation = line.split(",");
			topics.put(annotation[0], Arrays.asList(annotation).subList(1, annotation.length).toArray(new String [4]));
		}
		return topics;
	}
	
	private Instances loadInstances(HashMap<String,String> files, HashMap<String,HashMap<String,Annotation>> annotations, HashMap<String,String []> demographicArea, String name, boolean train) {
		Instances instances = new Instances(name, _weka.buildAttributes(), 0);
		
		int count = 0;
		int numAuthors = 0;
		int numPosts = 0;
		int numClaims = 0;
		int numAgreements = 0;
		int numDisagreements = 0;
		int numArgumentations = 0;
		int numPersuasions = 0;
		int numInfluencers = 0;
		int noInfluencers = 0;
		int [] gender = {0,0};
		int [] religion = {0,0,0,0};
		int [] politicalParty = {0,0};
		int [] age = {0,0};
		
		
		for (String file : files.keySet()) {
			if (!file.endsWith(".xml")) continue;
			count++;
			
			if (file.indexOf("Death__Archive_4___Images") >= 0) {
				System.out.println("check...");
			}

			String domain = files.get(file);
			String [] topics = null;
			if (domain.indexOf("wikipedia") >= 0) topics = demographicArea.get(file.substring(0,file.indexOf("__")));
			String processed = _processingDirectory + "/" + domain + "/processed-" + file + "/" + file;
			
			HashMap<String,ArrayList<Object>> claims = null;
			HashMap<String,String> agreements = null;
			HashMap<String,ArrayList<Object>> argumentations = null;
			HashMap<String,ArrayList<Object>> persuasions = null;
			HashMap<String,DialogFeatures> dialogs = null;
			DocumentDemographics demographics = null;
			HashMap<String,CredibilityFeatures> credibility = null;
			HashMap<String,Author> authors = getAuthorPostList(processed + ".sentences", annotations.get(file));
			
			numAuthors += authors.size();
			
			if (_claimDetector != null) {
				claims = _claimDetector.readByPost(processed + ".claims");
				
				for (String author : claims.keySet()) {
					numClaims += claims.get(author).size();
				}
			}
			if (_agreementDetector != null) {
				agreements = _agreementDetector.readByPost(processed + ".agreement");
				
				for (String key : agreements.keySet()) {
					if (agreements.get(key).equals("agreement")) numAgreements++;
					if (agreements.get(key).equals("disagreement")) numDisagreements++;
				}
			}
			if (_argumentationDetector != null) {
				argumentations = _argumentationDetector.readByPost(processed + ".argument");
				
				for (String key : argumentations.keySet()) {
					numArgumentations += argumentations.get(key).size();
				}
			}
			if (_persuasionDetector != null) {
				persuasions = _persuasionDetector.readByPost(processed);
				
				for (String key : persuasions.keySet()) {
					numPersuasions += persuasions.get(key).size();
				}
			}
			if (_dialogModule != null) {
				dialogs = _dialogModule.readByAuthor(processed + ".sentences");
				
				boolean influencer = false;
				
				for (String author : dialogs.keySet()) {
					DialogFeatures f = dialogs.get(author);
					numPosts += f.numPosts();
					if (authors.get(author).isInfluencer()) {
						numInfluencers++;
						influencer = true;
					}
				}
				if (!influencer) {
					System.out.println("No Influencers in: " + file);
					System.out.println(annotations.get(file));
					noInfluencers++;
				}
			}
			if (_demographicModule != null) {
				demographics = _demographicModule.readByAuthor(processed);
				age[demographics.getMajority(demographics.getAge(), -1)]++;
				gender[demographics.getMajority(demographics.getGender(),-1)]++;
				politicalParty[demographics.getMajority(demographics.getPoliticalParty(),-1)]++;
				religion[demographics.getMajority(demographics.getReligion(),-1)]++;
			}
			if (_credibilityModule != null) {
				credibility = _credibilityModule.readByAuthor(processed + ".sentences");
			}
			
			System.out.println(count + ": " + file + " (" + authors.size() + " authors)");
			instances = _weka.buildInstances(file, domain, topics, train ? count : 10000+count, name, instances, authors, dialogs, claims, argumentations, agreements, persuasions, demographics, credibility, Blog.processBlog(processed + ".sentences"));
		}
		
		System.out.println("There are " + numClaims + " claims.");
		System.out.println("There are " + numArgumentations + " argumentations.");
		System.out.println("There are " + numPersuasions + " persuasions.");
		System.out.println("There are " + numAgreements + " agreements.");
		System.out.println("There are " + numDisagreements + " disagreements.");
		System.out.println("There are " + files.size() + " disussions.");
		System.out.println("There are " + numAuthors + " authors.");
		System.out.println("There are " + numPosts + " posts.");
		System.out.println("There are " + numInfluencers + " influencers.");
		System.out.println("There are " + noInfluencers + " files without influencers.");
		System.out.println("There are " + Arrays.toString(age) + " age majorities.");
		System.out.println("There are " + Arrays.toString(gender) + " gender majorities.");
		System.out.println("There are " + Arrays.toString(politicalParty) + " political party majorities.");
		System.out.println("There are " + Arrays.toString(religion) + " religion majorities.");
		return instances;
	}
	
	/**
	 * Given a blog, generate a list of authors and their corresponding posts
	 * @param file
	 * @return
	 */
	private HashMap<String,Author> getAuthorPostList(String file, HashMap<String,Annotation> influencers) {
		
		HashMap<String, Author> authors = new HashMap<String, Author>();
		
		try {
			Blog blog = Blog.processBlog(file);
			Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
			
			Author a = new Author(blog.getUsername(),influencers != null ? influencers.containsKey(blog.getUsername().toLowerCase()) : false);
			a.addPost("-1");
			authors.put(blog.getUsername(), a);
			
			for (String key : entry.getCommentKeys()) {
				Comment comment = (blog.threaded.Comment)entry.getComment(key);
				
				a = authors.containsKey(comment.getUsername()) ? authors.get(comment.getUsername()) : 
					new Author(comment.getUsername(),influencers != null ? influencers.containsKey(comment.getUsername().toLowerCase()) : false);
				a.addPost(comment.getURL());
				authors.put(comment.getUsername(), a);
			}
		} catch (Exception e) {
			System.err.println("[Influencer.getAuthorPostList] Error on: " + file);
			e.printStackTrace();
		}
		return authors;
	}
		
	/**
	 * Perform preprocessing on all the files. Use threading to improve speed.
	 * @param directory
	 * @param files
	 */
	public void process(String directory, String type, String [] files) {
		System.out.println("[Influencer.process] Running Modules on " + directory);
		
		int numThreads = 1; //Runtime.getRuntime().availableProcessors()/4;
		
		System.out.println("[Influencer.process] Processing " + numThreads + 
				" files simultaneously. Processing " + files.length + " files");
		
		ExecutorService executor = Executors.newFixedThreadPool(numThreads);
		
		for (int i = 0; i < files.length; i++) {
			
			try {
				Runnable thread = new ProcessThread(directory, type, files[i], i);
				executor.execute(thread);
			} catch (Exception e) {
				System.err.println("Error on " + files[i]);
				e.printStackTrace();
			}
		}
		executor.shutdown();
        while (!executor.isTerminated()) {}

	}

	class ProcessThread implements Runnable {
		 private String _directory;
		 private String _file;
		 private int _index;
		 private String _type;
		   
		   ProcessThread(String directory, String type, String file, int index){
			   _directory = directory;
			   _file = file;
			   _index = index;
			   _type = type;
		   }

		   /**
		    * This is where the pre-processing actually happens for each file. 
		    * It runs the following:
		    * 1. claim
		    * 2. agreement
		    * 3. argumentation
		    * 4. demographics
		    */
		   public void run() {
		      try {
		    	  System.out.println(_index + ". Processing: " + _file);
					
					String processing = _processingDirectory + "/" + _type + "/processed-" + _file + "/" + _file;
					String path = _directory + "/" + _file;
					
					if (!_file.endsWith(".xml")) return;
					
					if (_claimDetector != null && !new File(processing + ".claims").exists()) {
						_claimDetector.run(path, _type);
					}
					if (_agreementDetector != null && !new File(processing + ".agreement").exists()) {
						_agreementDetector.run(path,_type);
					}
					if (_argumentationDetector != null && !new File(processing + ".argument").exists()) {
						_argumentationDetector.run(path,_type);
					}
					if (_demographicModule != null && (!new File(processing + ".gender").exists() || !new File(processing + ".year").exists() || 
									!new File(processing + ".age").exists() || !new File(processing + ".political_party").exists()  ||
									!new File(processing + ".political_view").exists() || !new File(processing + ".religion").exists()  )) {
						_demographicModule.run(path,_type);
					}
		     } catch (Exception e) {
		         System.out.println("Thread " +  _file + " interrupted.");
		     }
		   }
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		boolean livejournal_train = false;
		boolean livejournal = false;
		
		boolean wikipedia_train = false;
		boolean wikipedia = false;
		
		boolean politicalforum_train = false;
		boolean politicalforum = false;
		
		boolean createdebate_train = false;
		boolean createdebate = false;
		
		boolean twitter_train = false;
		boolean twitter = false;
		
		boolean domainAdaptation = false;
		
		boolean process = false;
		boolean exclude = false;
		
		int part = 0;
		int numparts = 1;
		
		boolean overwrite = false;
		
		String output = "createdebate";
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-train")) {
				String [] training = args[++i].split(",");
				
				for (String train : training) {
					if (train.equals("w")) wikipedia_train = true;
					else if (train.equals("l")) livejournal_train = true;
					else if (train.equals("p")) politicalforum_train = true;
					else if (train.equals("c")) createdebate_train = true;
					else if (train.equals("t")) twitter_train = true;
				}
			}
			
			else if (args[i].equals("-test")) {
				String [] testing = args[++i].split(",");
				
				for (String test : testing) {
					if (test.equals("w")) wikipedia = true;
					else if (test.equals("l")) livejournal = true;
					else if (test.equals("p")) politicalforum = true;
					else if (test.equals("c")) createdebate = true;
					else if (test.equals("t")) twitter = true;
				}
			}
			else if (args[i].equals("-domain")) {
				domainAdaptation = true;
			}
			else if (args[i].equals("-o")) {
				output = args[++i];
			}
			else if (args[i].equals("-part")) {
				String p [] = args[++i].split("/");
				part = Integer.valueOf(p[0])-1;
				numparts = Integer.valueOf(p[1]);
			}
			else if (args[i].equals("-overwrite")) {
				overwrite = true;
			}
			else if (args[i].equals("-exclude")) {
				exclude = true;
			}
		}
		
		// to do: all?
		String outputdirectory = "/proj/fluke/users/sara/influence/output/" + output + "/"; //-domain/";
		new File(outputdirectory).mkdirs();
		
		ArrayList<String> domains = new ArrayList<String>();
		if (livejournal || livejournal_train) domains.add("livejournal");
		if (wikipedia || wikipedia_train) domains.add("wikipedia");
		if (politicalforum || politicalforum_train) domains.add("political_forum");
		if (createdebate || createdebate_train) domains.add("create_debate");
		if (twitter || twitter_train) domains.add("twitter");

		String corpora = "/proj/fluke/SCIL/corpora";
		
		Influencer influencerDetector = new Influencer(domains,false, false, false, false,false,true,false,
						true,false, overwrite, process, part, numparts);
		//Influencer influencerDetector = new Influencer(domains,true, true, true, true,true,true,true,
		//		false,false, overwrite, process, part, numparts);
		//Influencer influencerDetector = new Influencer(domains,true, true, false, false,false,false,false, false, true);
		
		//String trainDirectory = "/proj/fluke/SCIL/data/influencer/sc-annotated-lj-july2011/";
		//String testDirectory = "/proj/fluke/SCIL/data/influencer/midterm-2011-lj-annotated/"; //wiki-10/";
		//String trainDirectory = "/proj/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/";
		//String testDirectory = "/proj/fluke/SCIL/data/influencer/wiki-10/";
		//String trainDirectory = "/proj/fluke/SCIL/data/PoP-Influencer/train/";
		
		HashMap<String,String> files = new HashMap<String,String>();
		HashMap<String,HashMap<String,Annotation>> annotations = new HashMap<String,HashMap<String,Annotation>>();

		HashMap<String,String []> topics = new HashMap<String,String []>();
		ArrayList<String> trainnames = new ArrayList<String>();
		
		//if (overwrite || !new File(outputdirectory + "/influence-train.arff").exists()) {
			if (wikipedia_train) {
				String trainDirectory = "/proj/fluke/users/sara/influence/annotations/train/influence/wikipedia";
				//String testDirectory = "/proj/fluke/users/sara/influence/annotations/dev/influence/wikipedia";
				
				topics = influencerDetector.loadDemographicArea("/proj/fluke/users/sara/corpora/discussion_forums/wikipedia/topics/demographic-issues.csv");
				
				// Wikipedia
				HashMap<String,HashMap<String,Annotation>> wikiAnnotations = influencerDetector.loadAnnotations(trainDirectory + "/influence_train.csv","wikipedia");
				annotations.putAll(wikiAnnotations);
				//influencerDetector.loadFailedPOPs(trainDirectory + "/influencers.csv", trainDirectory + "/pop.csv");
				ArrayList<String> files1 = influencerDetector.loadFilesFromAnnotations(corpora + "/wikipedia/WD_batch1LJ2/", annotations.keySet());
				
				for (String file : files1)
					files.put(file,"wikipedia");
				if (process) influencerDetector.process(corpora + "/wikipedia/WD_batch1LJ2/","wikipedia",files1.toArray(new String[files1.size()]));
				ArrayList<String> files2 = influencerDetector.loadFilesFromAnnotations(corpora + "/wikipedia/WD_batch2_year3LJ/", annotations.keySet());
				if (process) influencerDetector.process(corpora + "/wikipedia/WD_batch2_year3LJ/","wikipedia",files2.toArray(new String[files2.size()]));
				for (String file : files2)
					files.put(file,"wikipedia");
				trainnames.add("wikipedia");
			}
			if (livejournal_train) {
				// livejournal
				String trainDirectory = "/proj/fluke/users/sara/influence/annotations/train/influence/livejournal";
				HashMap<String,HashMap<String,Annotation>> ljAnnotations = influencerDetector.loadAnnotations(trainDirectory + "/influence_train.csv","livejournal");
				annotations.putAll(ljAnnotations);
				ArrayList<String> files1 = influencerDetector.loadFilesFromAnnotations("/proj/fluke/SCIL/corpora/livejournal/agreement-batch1", ljAnnotations.keySet());
				for (String file : files1)
					files.put(file,"livejournal");
				if (process) influencerDetector.process("/proj/fluke/SCIL/corpora/livejournal/agreement-batch1","livejournal",files1.toArray(new String[files1.size()]));
				trainnames.add("livejournal");
			}
			if (politicalforum_train) {
				String trainDirectory = "/proj/fluke/users/sara/influence/annotations/train/influence/political_forum/influence_train.csv";
				HashMap<String,HashMap<String,Annotation>> pfAnnotations = influencerDetector.loadAnnotations(trainDirectory,"political_forum");
				annotations.putAll(pfAnnotations);
				ArrayList<String> files1 = influencerDetector.loadFilesFromAnnotations("/proj/fluke/corpora/politics/political-forumLJ/republican-primary/", pfAnnotations.keySet());
				if (process) influencerDetector.process("/proj/fluke/corpora/politics/political-forumLJ/republican-primary/","political_forum",files1.toArray(new String[files1.size()]));
				for (String file : files1)
					files.put(file,"political_forum");
				trainnames.add("political_forum");
			}
			if (twitter_train) {
				String trainDirectory = "/proj/fluke/users/sara/influence/annotations/train/influence/twitter/influence_train.csv";
				HashMap<String,HashMap<String,Annotation>> twitterAnnotations = influencerDetector.loadAnnotations(trainDirectory,"twitter");
				annotations.putAll(twitterAnnotations);
				ArrayList<String> files1 = influencerDetector.loadFilesFromAnnotations("/proj/fluke/corpora/politics/twitter/republican-primary/", twitterAnnotations.keySet());
				if (process) influencerDetector.process("/proj/fluke/corpora/politics/twitter/republican-primary/", "twitter", files1.toArray(new String[files1.size()]));
				for (String file : files1)
					files.put(file,"twitter");
				trainnames.add("twitter");
			}
			if (createdebate_train) {
				String trainDirectory = "/proj/fluke/users/sara/influence/annotations/train/influence/create_debate/influence_train.csv";
				HashMap<String,HashMap<String,Annotation>> cdAnnotations = influencerDetector.loadAnnotations(trainDirectory,"create_debate");
				annotations.putAll(cdAnnotations);
				ArrayList<String> files1 = influencerDetector.loadFilesFromAnnotations("/proj/fluke/corpora/politics/create-debate/republican-primary/", cdAnnotations.keySet());
				if (process) influencerDetector.process("/proj/fluke/corpora/politics/create-debate/republican-primary/", "create_debate", files1.toArray(new String[files1.size()]));
				for (String file : files1)
					files.put(file,"create_debate");
				trainnames.add("create_debate");
			}
		//}
		// testing
		// load annotations and process files
		ArrayList<String> testnames = new ArrayList<String>();
		String testDirectory = "";
		ArrayList<HashMap<String,String>> testFiles = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String,HashMap<String,Annotation>>> testAnnotations = new ArrayList<HashMap<String,HashMap<String,Annotation>>>();
		if (wikipedia) {
			String t = "test";
			testDirectory = "/proj/fluke/users/sara/influence/annotations/" + t + "/influence/wikipedia/influence_" + t + ".csv";	
			testAnnotations.add(influencerDetector.loadAnnotations(testDirectory,"wikipedia"));
			// Wikipedia Test
			ArrayList<String> testFiles1 = influencerDetector.loadFilesFromAnnotations(corpora + "/wikipedia/WD_batch1LJ2/", testAnnotations.get(testAnnotations.size()-1).keySet());
			
			HashMap<String,String> tf = new HashMap<String,String>();
			for (String file : testFiles1)
				tf.put(file,"wikipedia");
			if (process) influencerDetector.process(corpora + "/wikipedia/WD_batch1LJ2/", "wikipedia", testFiles1.toArray(new String[testFiles1.size()]));
			ArrayList<String> testFiles2 = influencerDetector.loadFilesFromAnnotations(corpora + "/wikipedia/WD_batch2_year3LJ/", testAnnotations.get(testAnnotations.size()-1).keySet());
			if (process) influencerDetector.process(corpora + "/wikipedia/WD_batch2_year3LJ/","wikipedia",testFiles2.toArray(new String[testFiles2.size()]));
			for (String file : testFiles2)
				tf.put(file,"wikipedia");
			testFiles.add(tf);
			testnames.add("wikipedia");
		}
		if (politicalforum) {
			testDirectory = "/proj/fluke/users/sara/influence/annotations/test/influence/political_forum/influence_test.csv";
			testAnnotations.add(influencerDetector.loadAnnotations(testDirectory,"political_forum"));
			ArrayList<String> testFiles1 = influencerDetector.loadFilesFromAnnotations("/proj/fluke/corpora/politics/political-forumLJ/republican-primary/", testAnnotations.get(testAnnotations.size()-1).keySet());
			if (process) influencerDetector.process("/proj/fluke/corpora/politics/political-forumLJ/republican-primary/","political_forum",testFiles1.toArray(new String[testFiles.size()]));
			
			HashMap<String,String> tf = new HashMap<String,String>();
			for (String file : testFiles1)
				tf.put(file,"political_forum");
			testFiles.add(tf);
			testnames.add("political_forum");
		}
		if (twitter) {
			testDirectory = "/proj/fluke/users/sara/influence/annotations/test/influence/twitter/influence_test.csv";
			testAnnotations.add(influencerDetector.loadAnnotations(testDirectory,"twitter"));
			ArrayList<String> testFiles1 = influencerDetector.loadFilesFromAnnotations("/proj/fluke/corpora/politics/twitter/republican-primary/", 
					testAnnotations.get(testAnnotations.size()-1).keySet());
			if (process) influencerDetector.process("/proj/fluke/corpora/politics/twitter/republican-primary/", "twitter", testFiles1.toArray(new String[testFiles1.size()]));
			
			HashMap<String,String> tf = new HashMap<String,String>();
			for (String file : testFiles1)
				tf.put(file,"twitter");
			testnames.add("twitter");
			testFiles.add(tf);
		}
		if (createdebate) {
			testDirectory = "/proj/fluke/users/sara/influence/annotations/test/influence/create_debate/influence_test.csv";
			testAnnotations.add(influencerDetector.loadAnnotations(testDirectory,"create_debate"));
			ArrayList<String> testFiles1 = influencerDetector.loadFilesFromAnnotations(
					"/proj/fluke/corpora/politics/create-debate/republican-primary/", testAnnotations.get(testAnnotations.size()-1).keySet());
			if (process) influencerDetector.process("/proj/fluke/corpora/politics/create-debate/republican-primary/", "create_debate", testFiles1.toArray(new String[testFiles1.size()]));
			
			HashMap<String,String> tf = new HashMap<String,String>();
			for (String file : testFiles1)
				tf.put(file,"create_debate");
			testnames.add("create_debate");
			testFiles.add(tf);
		}
		if (livejournal) {
			testDirectory = "/proj/fluke/users/sara/influence/annotations/test/influence/livejournal/influence_test.csv";
			testAnnotations.add(influencerDetector.loadAnnotations(testDirectory,"livejournal"));
			ArrayList<String> testFiles1 = influencerDetector.loadFilesFromAnnotations(
					"/proj/fluke/SCIL/corpora/livejournal/agreement-batch1", testAnnotations.get(testAnnotations.size()-1).keySet());
			if (process) influencerDetector.process("/proj/fluke/SCIL/corpora/livejournal/agreement-batch1","livejournal",testFiles1.toArray(new String[testFiles1.size()]));
			
			HashMap<String,String> tf = new HashMap<String,String>();
			for (String file : testFiles1)
				tf.put(file,"livejournal");
			testnames.add("livejournal");
			testFiles.add(tf);
		}
		
		//4. classify
		//influencerDetector.crossvalidation(trainDirectory, annotations, files.toArray(new String[files.size()]), "logistic",  10);
		
		//System.out.println("Training: ");
		//influencerDetector.statistics(files,annotations, topics, true);
		//System.out.println("Testing: ");
		//influencerDetector.statistics(testFiles.get(0),testAnnotations.get(0), topics, true);
		//System.exit(0);
		System.out.println("Training: " + trainnames);
		System.out.println("Testing: " + testnames);
		
		influencerDetector.run(files, testFiles, "svm", annotations, testAnnotations, 
				outputdirectory, "/proj/fluke/users/sara/influence/input/experiments.txt",
				exclude, domainAdaptation, topics, trainnames,testnames);
	}
// TP = 44 TN = 0 FP = 206 FN = 0
}
