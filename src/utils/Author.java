/**
 * 
 */
package utils;

import java.util.*;

/**
 * @author sara
 *
 */
public class Author {

	String _name;
	boolean _influencer;
	ArrayList<String> _posts;
	
	public Author(String name, boolean influencer) {
		_name = name;
		_influencer = influencer;
		_posts = new ArrayList<String>();
	}
	
	public String getName() {
		return _name;
	}
	
	public boolean isInfluencer() {
		return _influencer;
	}
	
	public ArrayList<String> getPosts() {
		return _posts;
	}
	
	public int numPosts() {
		return _posts.size();
	}
	
	public void addPost(String post) {
		_posts.add(post);
	}
	
	public String toString() {
		return _name + " (" + Arrays.toString(_posts.toArray()) + "): " + (_influencer ? "Y" : "N");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
