/**
 * 
 */
package utils;

/**
 * @author sara
 *
 */
public class DemographicFeatures {

	double _gender;
	double _year;
	double _politicalParty;
	double _politicalView;
	double _religion;
	double _age;

	public DemographicFeatures(double gender, double year, double age, double politicalParty, double politicalView, double religion) {
		_gender = gender;
		_year = year;
		_age = age;
		_politicalParty = politicalParty;
		_politicalView = politicalView;
		_religion = religion;
	}
	
	public DemographicFeatures(Double [] data) {
		this(data[0],data[1],data[2],data[3],data[4],data[5]);
	}
	
	public double getGender() {
		return _gender;
	}
	
	public boolean isMale() {
		return _gender == 0;
	}
	
	public double getYear() {
		return _year;
	}
	
	public double getBinaryAge() {
		return _age;
	}
	
	public double getPoliticalParty() {
		return _politicalParty;
	}
	
	public double getPoliticalView() {
		return _politicalView;
	}
	
	public double getReligion() {
		return _religion;
	}
	
	public double getAgeReligion() {
		return combine(_religion, _age);
	}
	
	public double getGenderReligion() {
		return combine(_religion,_gender);
	}
	
	public double getPoliticalPartyReligion() {
		return combine(_religion, _politicalParty);
	}
	
	public double getPoliticalViewReligion() {
		return combine(_religion,_politicalView);
	}
	
	public double getPoliticalViewAge() {
		return combine(_politicalView, _age);
	}
	
	public double getPoliticalViewGender() {
		return combine(_politicalView,_gender);
	}
	
	public double getPoliticalPartyAge() {
		return combine(_politicalParty, _age);
	}
	
	public double getPoliticalPartyGender() {
		return combine(_politicalParty,_gender);
	}
	
	public double getPolitics() {
		return combine(_politicalView, _politicalParty);
	}
	
	public double getAgeGender() {
		return combine(_age,_gender);
	}
	
	private double combine(double f1, double f2) {
		if (f1 == 0.0 && f2 == 0.0) return 0.0;
		if (f1 == 0.0 && f2 == 1.0) return 1.0;
		if (f1 == 1.0 && f2 == 0.0) return 2.0;
		if (f1 == 1.0 && f2 == 1.0) return 3.0;
		// religion
		if (f1 == 2.0 && f2 == 0.0) return 4.0;
		if (f1 == 2.0 && f2 == 1.0) return 5.0;
		if (f1 == 3.0 && f2 == 0.0) return 6.0;
		if (f1 == 3.0 && f2 == 1.0) return 7.0;
		return -1;
	}
	
	public String toString() {
		return _year + ", " + _age + ", " + _gender + ", " + _politicalParty + ", " + _politicalView + ", " + _religion;
	}
}
