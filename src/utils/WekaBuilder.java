package utils;

import java.io.File;
//import java.io.FileOutputStream;
import java.util.*;

import blog.threaded.livejournal.Blog;

import modules.*;
import app.*;

import weka.classifiers.*;
import weka.classifiers.bayes.*;
import weka.classifiers.functions.*;
import weka.classifiers.trees.*;
import weka.core.*;
import weka.core.converters.ArffSaver;
//import weka.core.converters.SVMLightSaver;
import weka.filters.unsupervised.attribute.StringToWordVector;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.*;//.PearsonsCorrelation;

import edu.columbia.opinion.io.ArffReader;
import edu.columbia.utilities.weka.ChiSquareFeatureSelection;
import edu.columbia.utilities.weka.InstancesProcessor;

public class WekaBuilder {
	
    ArrayList<Integer> _featureRanges;
    ArrayList<String> _featureNames;
    ArrayList<String> _domains;
    boolean _dialog, _demographics, _agreement, _argumentation, _claim, _persuasion, _credibility;
    boolean _exact, _topics;
    
    Attribute _influencer, _docId, _domain, _numWords;
    Attribute _genderTopic, _ageTopic, _religionTopic, _politicsTopic;
    
    boolean combo = true;
    boolean binary = true;
    boolean majority = true;
    
    boolean _overwrite = false;
    
	// _numparts must be multiple of numexperiments (power of 2)
    int _part = 0;
    int _numparts = 1;
	
	public WekaBuilder(ArrayList<String> domains, boolean claim, boolean argumentation, boolean persuasion, boolean agreement, boolean dialog, 
			boolean demographics, boolean credibility, boolean topics, boolean exact, boolean overwrite, int part, int numparts) {
		_exact = exact;
		
		_dialog = dialog;
		_demographics = demographics;
		_agreement = agreement;
		_argumentation = argumentation;
		_claim = claim;
		_persuasion = persuasion;
		_credibility = credibility;
		
		_domains = domains;
		_topics = topics;
		
		_part = part;
		_numparts = numparts;
		
		_overwrite = overwrite;
		
		ArrayList<String> values = new ArrayList<String>(2);
		values.add("N");
		values.add("Y");
		_influencer = new Attribute("influencer", values);
		_docId = new Attribute("qid");
		_numWords = new Attribute("num_words");
		values = new ArrayList<String>(domains.size());
		for (String domain : domains)
			values.add(domain);
		_domain = new Attribute("domain", values);
		_genderTopic = new Attribute("gender_topic");
		_ageTopic = new Attribute("age_topic");
		_religionTopic = new Attribute("religion_topic");
		_politicsTopic = new Attribute("politics_topic");
		
	}
	
	public ArrayList<Attribute> buildAttributes() {
		_featureRanges = new ArrayList<Integer>();
		_featureNames = new ArrayList<String>();
		
		ArrayList<Attribute> attributes = new ArrayList<Attribute>();
		attributes.add(_docId);
		attributes.add(_domain);
		attributes.add(_numWords);
		
		if (_claim) {
			ClaimModule.buildAttributes(attributes, _featureRanges, _featureNames);
		}
		if (_argumentation) {
			ArgumentationModule.buildAttributes(attributes, _featureRanges, _featureNames);
		}
		if (_persuasion) {
			PersuasionModule.buildAttributes(attributes, _featureRanges, _featureNames);
		}
		if (_agreement) {
			AgreementModule.buildAttributes(attributes, _featureRanges, _featureNames);
		}
		if (_dialog) {
			DialogModule.buildAttributes(attributes, _featureRanges, _featureNames);
		}
		if (_demographics) {
			DemographicModule.buildAttributes(attributes, _featureRanges, _featureNames, combo, majority, binary);
		}
		if (_credibility) {
			CredibilityModule.buildAttributes(attributes, _featureRanges, _featureNames);
		}
		if (_topics) {
			attributes.add(_genderTopic);
			attributes.add(_ageTopic);
			attributes.add(_religionTopic);
			attributes.add(_politicsTopic);
		}
	
		attributes.add(_influencer);
		return attributes;
	}
	
	public Instances buildInstances(String file, String domain, String [] topics, int id, String name, Instances instances, HashMap<String,Author> authors, HashMap<String,DialogFeatures> dialogs, HashMap<String,ArrayList<Object>> claims, HashMap<String,ArrayList<Object>> argumentations, 
			HashMap<String,String> agreements, HashMap<String,ArrayList<Object>> persuasions, DocumentDemographics demographics, HashMap<String, CredibilityFeatures> credibility, Blog blog) {
		
		// testing just religion
		//if (Integer.valueOf(topics[2]) <= 5) return instances;
		
		double numPosts = 0;
		double numClaims = 0;
		double numArgumentations = 0;
		double numPersuasions = 0;
		double numSentences = 0;
		
		for (String key : authors.keySet()) {
			numPosts += authors.get(key).numPosts();
			numSentences += DialogFeatures.numSentences(blog, key);
		}
		
		/*if (agreements != null) {
			for (String key : agreements.keySet()) {
				if (!agreements.get(key).equals("none")) numAgreements++;
				if (!agreements.get(key).equals("none")) numDisagreements++;
			}
		}*/
		
		if (claims != null) {
			for (String key : claims.keySet()) {
				numClaims += claims.get(key).size();
			}
		}
		
		if (argumentations != null) {
			for (String key : argumentations.keySet()) {
				numArgumentations += argumentations.get(key).size();
			}
		}
		
		if (persuasions != null) {
			for (String key : persuasions.keySet()) {
				numPersuasions += persuasions.get(key).size();
			}
		}
		
		for (String key : authors.keySet()) {
			Author author = authors.get(key);
			
			int numAttributes = 4;
			if (_topics) numAttributes += 4;
			if (_claim) numAttributes +=  ClaimModule.getExperiments(true).size();
			if (_argumentation) numAttributes+= ArgumentationModule.getExperiments(true).size();
			if (_agreement) numAttributes += AgreementModule.getExperiments(true).size();
			if (_persuasion) numAttributes += PersuasionModule.getExperiments(true).size();
			if (_dialog) numAttributes+= DialogModule.getExperiments(true).size();
			if (_demographics) numAttributes+= DemographicModule.numExperiments(true, true, true); //combo, majority, binary);
			if (_credibility) numAttributes+= CredibilityModule.getExperiments(true).size();
			
			Instance instance = new DenseInstance(numAttributes);
			
			if (_claim) {
				ClaimModule.setInstance(instance, claims, author, blog, numClaims, numPosts, numSentences);
			}
			if (_argumentation) {
				ArgumentationModule.setInstance(instance,argumentations, author, blog, numArgumentations, numPosts, numSentences);
			}
			if (_persuasion) {
				PersuasionModule.setInstance(instance, persuasions, author, blog, numPersuasions, numPosts, numSentences);
			}
			if (_agreement) {
				AgreementModule.setInstance(instance,agreements, author, blog, numPosts);				
			}
			if (_demographics) {
				DemographicModule.setInstance(instance, demographics, author.getName(), combo, majority, binary);
			}
			if (_credibility) {
				CredibilityModule.setInstance(instance, credibility.get(author.getName()));
			}
			if (_dialog) {
				DialogFeatures df = dialogs.get(author.getName());
				DialogModule.setInstance(instance, df, (int)numPosts);
			}
			if (_topics) {
				instance.setValue(_genderTopic, Integer.valueOf(topics[0]) > 5 ? 1 : 0);
				instance.setValue(_ageTopic, Integer.valueOf(topics[1]) > 5 ? 1 : 0);
				instance.setValue(_religionTopic, Integer.valueOf(topics[2]) > 5 ? 1 : 0);
				instance.setValue(_politicsTopic, Integer.valueOf(topics[3]) > 5 ? 1 : 0);
			}
			//if (author.isInfluencer()) System.err.println(author.getName() + " is the influencer.");
			instance.setValue(_influencer, author.isInfluencer() ? "Y" : "N");
			instance.setValue(_docId, id);
			instance.setValue(_numWords, DialogFeatures.numWords(blog, author.getName()));
			instance.setValue(_domain, domain);
			if(name.equals("testing") && Influencer.DEBUG) System.out.println(file + "," + author.getName() + "," + instance.toString());
			instances.add(instance);
		}
		instances.setClass(_influencer);
		return instances;
	}
	
	/*private int addExperiments(AbstractModule module, List<String> experiments) {
		ArrayList<String> e = module.getExperiments(_exact);
		experiments.addAll(e);
		return e.size();
	}*/
	
	/*private void svmLightSaver(String output, Instances instances) {
		try {
			SVMLightSaver saver = new SVMLightSaver();
			saver.setInstances(instances);
			saver.setFile(new File(output));
			saver.writeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	private void arffSaver(String output, Instances instances) {
		try {
			ArffSaver saver = new ArffSaver();
			saver.setInstances(instances);
			saver.setFile(new File(output));
			saver.writeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Instances arffLoader(String input) {
		try {
			//BufferedReader reader = new BufferedReader(new FileReader(input));
			ArffReader arff = new ArffReader(input);
			Instances data = arff.getInstances();
			data.setClassIndex(data.numAttributes() - 1);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	private HashMap<String,Instances> featureSelection(Instances fullTrain, HashMap<String,Instances> fullTest, ArrayList<String> genreNames) {
		if (_claim) {
			HashSet<String> claimGrams = null;
			
			List<Object> modified = computeNGrams(fullTrain,claimGrams,null,"claim_text", true, genreNames);
			fullTrain = (Instances) modified.get(0); 
			claimGrams = (HashSet<String>) modified.get(1);
			fullTest = computeNGramsTest(fullTest,claimGrams,"claim_text");
		}
		if (_argumentation) {
			HashSet<String> argumentationGrams = null;
			List<Object> modified = computeNGrams(fullTrain,argumentationGrams,null,"argumentation_text", true, genreNames);
			fullTrain = (Instances) modified.get(0); 
			argumentationGrams = (HashSet<String>) modified.get(1);
			fullTest = computeNGramsTest(fullTest,argumentationGrams,"argumentation_text");
		}
		/*if (_demographics) {
			HashSet<String> demographicGrams = null;
			List<Object> modified = computeNGrams(fullTrain,fullTest, demographicGrams,null,"demographic", false);
			fullTrain = (Instances) modified.get(0); demographicGrams = (HashSet<String>) modified.get(1);
			fullTest = (Instances) computeNGrams(fullTrain, fullTest,demographicGrams,(StringToWordVector)modified.get(2),"demographic",false).get(0);
		}
		if (_dialog) {
			HashSet<String> demographicGrams = null;
			List<Object> modified = computeNGrams(fullTrain,fullTest, demographicGrams,null,"dialog", false);
			fullTrain = (Instances) modified.get(0); demographicGrams = (HashSet<String>) modified.get(1);
			fullTest = (Instances) computeNGrams(fullTrain, fullTest,demographicGrams,(StringToWordVector)modified.get(2),"dialog",false).get(0);
		}*/
		HashMap<String,Instances> l = new HashMap<String,Instances>();
		l.put("train",fullTrain);
		l.putAll(fullTest);
		return l;
	}
		
	public HashMap<String,ArrayList<Double>> classify(String classifierName, HashMap<String,double [][]> classifications, 
			Instances fullTrain, HashMap<String,Instances> fullTest, String outputDir, String experimentFile, boolean domainAdaptation, 
			boolean ranking, ArrayList<String> domainNamesTrain, ArrayList<String> domainNamesTest) {
				
		Classifier classifier = WekaBuilder.getClassifier(classifierName);
		HashMap<String,ArrayList<Double>> evaluations = new HashMap<String,ArrayList<Double>>();
    	
    	try {
			int counter = 0;
			List <String> experiments = new ArrayList<String>();
			if (_claim) { List<String> e = ClaimModule.getExperiments(_exact); counter += e.size(); experiments.addAll(e); }
			if (_persuasion) {List<String> e = PersuasionModule.getExperiments(_exact); counter += e.size(); experiments.addAll(e);}
			if (_dialog) { List<String> e = DialogModule.getExperiments(_exact); counter += e.size(); experiments.addAll(e); }
			if (_demographics) { List<String> e = DemographicModule.getExperiments(_exact,combo, majority, binary); counter += e.size(); experiments.addAll(e); }
			if (_agreement) { List<String> e =  AgreementModule.getExperiments(_exact); counter += e.size(); experiments.addAll(e); }
			if (_argumentation) { List<String> e =  ArgumentationModule.getExperiments(_exact); counter += e.size(); experiments.addAll(e); }
			if (_credibility) { List<String> e =  CredibilityModule.getExperiments(_exact); counter += e.size(); experiments.addAll(e); }
			//if (_topics) { counter += 1; experiments.add("topic"); }
			
			String experimentName = "";
			
			fullTrain = pseudoBalance(fullTrain);
			int [] stats = fullTrain.attributeStats(fullTrain.classIndex()).nominalCounts;
			System.out.println("Class Breakdown Training: " + Arrays.toString(stats));
			
			for (String key : fullTest.keySet()) {
				Instances test = fullTest.get(key);
				stats = test.attributeStats(test.classIndex()).nominalCounts;
				System.out.println(key + " Class Breakdown Testing: " + Arrays.toString(stats));
			}
			if ((outputDir != null && _overwrite) || !(new File(outputDir + "/influence-train.arff").exists())) {
				//svmLightSaver(outputDir + "/influence-train.dat",fullTrain);
				//svmLightSaver(outputDir + "/influence-test.dat",fullTest);
				arffSaver(outputDir + "/influence-train.arff",fullTrain);
				//arffSaver(outputDir + "/influence-test.arff",fullTest);
			}
			
			HashMap<String,Instances> lists = featureSelection(fullTrain, fullTest, domainAdaptation ? domainNamesTrain : null);
			fullTrain = lists.get("train"); 
			lists.remove("train");
			fullTest = lists;
			//fullTest = new ArrayList<Instances>();
			//for (int i = 1; i < lists.size(); i++)
			//	fullTest.add(lists.get(i));
			
			for (int i = 0; i < fullTrain.numInstances(); i++) {
				if (fullTrain.instance(i).hasMissingValue()) {
					fullTrain.instance(i).replaceMissingValues(new double [fullTrain.numAttributes()]);
					// System.out.println("Missing:" + fullTrain.instance(i).toString());
				}
			}
			
			for (String key : fullTest.keySet()) {
				for (int j = 0; j < fullTest.get(key).numInstances(); j++) {
					if (fullTest.get(key).instance(j).hasMissingValue()) {
						fullTest.get(key).instance(j).replaceMissingValues(new double [fullTest.get(key).numAttributes()]);
						//System.out.println("Missing: " + fullTest.instance(i).toString());
					}
				}
			}
			
			if (domainAdaptation) {
				
				HashSet<String> exclude = new HashSet<String>();
				exclude.add("qid");
				exclude.add("domain");
				exclude.add("influencer");
				
				if ((outputDir != null && _overwrite) || !(new File(outputDir + "/influence-domain-train.arff").exists())) {
					System.out.println("[WekaBuilder.classify] Applying domain adaptation to train...");
					
					fullTrain = edu.columbia.utilities.weka.DomainAdaptation.easyDomainAdaptation(fullTrain, _domains, exclude);
					edu.columbia.utilities.weka.InstancesProcessor.reorder(fullTrain, fullTrain.attribute("influencer").index());
					
					System.out.print("Applying domain adaptation to test...");
					for (String key : fullTest.keySet()) {
						fullTest.put(key, edu.columbia.utilities.weka.DomainAdaptation.easyDomainAdaptation(fullTest.get(key), _domains, exclude));
						edu.columbia.utilities.weka.InstancesProcessor.reorder(fullTest.get(key), fullTest.get(key).attribute("influencer").index());
					}				
					arffSaver(outputDir + "/influence-domain-train.arff",fullTrain);
					//arffSaver(outputDir + "/influence-domain-test.arff",fullTest);
				}
				else {
					System.out.println("Loading domain adaptation...");
					fullTrain = arffLoader(outputDir + "/influence-domain-train.arff");
					
					System.out.print("Applying domain adaptation to test...");
					for (String key : fullTest.keySet()) {
						fullTest.put(key, edu.columbia.utilities.weka.DomainAdaptation.easyDomainAdaptation(fullTest.get(key), _domains, exclude));
						edu.columbia.utilities.weka.InstancesProcessor.reorder(fullTest.get(key), fullTest.get(key).attribute("influencer").index());
					}				
				}
				System.out.println(" DONE");
			}
			
			if (_part == 0) {
				// baseline
				System.out.println("numwords_baseline");
				evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, 
						new HashSet<String>(), "numwords_baseline", classifications, ranking,domainNamesTest));
				
				// single
				for (int index = 0; index < experiments.size(); index++) {
					HashSet<String> experimentList = new HashSet<String>();
					experimentList.add(experiments.get(index));
					experimentName = experiments.get(index);
					System.out.println(experimentName);
					evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, experimentList, experimentName, 
							classifications, ranking,domainNamesTest));
					//evaluations.put(experimentName,predictions.get(0));
					//if (ranking) evaluations.put(experimentName + "_ranking",predictions.get(1));
				}
				
				//HashSet<String> experimentList = new HashSet<String>(experiments);
				
				// all
				/*experimentName = Arrays.toString(experimentList.toArray()).replaceAll(", ", "-").substring(1);
				System.out.println(experimentName);
				evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, experimentList,experimentName, 
						classifications, ranking, domainNamesTest));
				//evaluations.put(experimentName,predictions.get(0));*/
				//if (ranking) evaluations.put(experimentName + "_ranking",predictions.get(1));
			}
			if (experimentFile != null) {
				List<String> experimentsFromFile = processing.GeneralUtils.readLines(experimentFile);
				int numexperiments = experimentsFromFile.size();
				
				if (_numparts > numexperiments) _numparts = numexperiments;
								
				for (int index = _part; index < _numparts; index++) {
					String experiment = experimentsFromFile.get(index);
					
					// comment
					if (experiment.startsWith("#") || experiment.trim().isEmpty()) continue;
					
					experimentName = experiment;
					String [] list = experiment.split("-");
					HashSet<String> hashList = new HashSet<String>();
					for (String l : list) {
						hashList.add(l);
					}
					
					System.out.println(index + "/" + _numparts + ":" + experimentName);
					evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, hashList, experimentName, 
							classifications, ranking, domainNamesTest));
					//evaluations.put(experimentName,predictions.get(0));
					//if (ranking) evaluations.put(experimentName + "_ranking",predictions.get(1));
				}
			}
			
			if (!_exact || _exact) {
				double numexperiments = Math.pow(2,  counter);
				System.out.println("Running " + (int)numexperiments + " experiments.");
				
				if (_part == 0) {
					// baseline
					System.out.println("numwords_baseline");
					evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, 
							new HashSet<String>(), "numwords_baseline", classifications, ranking,domainNamesTest));
				}
				// single
				HashSet<String> experimentList = new HashSet<String>(experiments);
				
				/*for (int index = 0; index < experiments.size(); index++) {
					experimentList = new HashSet<String>();
					experimentList.add(experiments.get(index));
					experimentName = experiments.get(index);
					evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, experimentList, experimentName, 
							classifications, ranking, domainNames));
					//evaluations.put(experimentName,predictions.get(0));
					//if (ranking) evaluations.put(experimentName + "_ranking",predictions.get(1));
				}*/
				
				// all
				/*experimentName = Arrays.toString(experimentList.toArray()).replaceAll(", ", "-").substring(1);
				evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, experimentList, experimentName, 
						classifications, ranking, domainNames));*/
				//evaluations.put(experimentName,predictions.get(0));
				//if (ranking) evaluations.put(experimentName + "_ranking",predictions.get(1));
				
				// ability to run portion of experiments at a time
				System.out.println(((int)numexperiments/_numparts)*_part + "-" + ((int)numexperiments/_numparts)*(_part+1));
				
				for (int index = ((int)numexperiments/_numparts)*_part; index < ((int)numexperiments/_numparts)*(_part+1); index++) {
		
					try {
						String experiment = Integer.toBinaryString(index);
						while (experiment.length() < counter) experiment = "0" + experiment;
					
						experimentName = "influence-";
						// binary+majority
						//experimentName += //"demographic_binary_age_binary-demographic_binary_gender-demographic_binary_religion-demographic_majority_age-demographic_majority_religion-demographic_majority_political_party-demographic_majority_all-demographic_majority_none";
						//"demographic_binary_age_exact-demographic_binary_age_distance-demographic_binary_gender-demographic_binary_religion";
						//experimentName += "demographic_binary_age_binary-demographic_binary_age_distance-demographic_binary_religion-demographic_majority_gender-demographic_majority_religion-demographic_majority_political_party-demographic_majority_all-demographic_majority_none";
						// majority
						//experimentName += "demographic_majority_age-demographic_majority_gender-demographic_majority_religion-demographic_majority_political_party-demographic_majority_all-demographic_majority_none";
						// binary
						//experimentName += "demographic_binary_gender-demographic_binary_religion-demographic_binary_political_party";
						experimentList = new HashSet<String>();
						
						for (int i = 0; i < experiments.size(); i++) {
							if (experiment.charAt(i) == '1') {
								experimentName += "-" + experiments.get(i);
								experimentList.add(experiments.get(i));
							}
						}
						if (experimentList.size() == 1) experimentName = "single---" + experimentName;
						if (experimentList.size() == experiments.size()) 
							experimentName = "all---" + experimentName;
						if (experimentName.indexOf("credibility") == -1) continue;
						System.out.println(index + "/" + (((int)numexperiments/_numparts)*(_part+1)) + ":" + experimentName);
						
						//if (!Influencer.DEBUG && experimentList.size() < counter-1 && experimentList.size() > 1) continue;
						//if (Influencer.DEBUG && experimentList.size() < counter) continue;
						//System.out.println(index + "/" + Math.pow(2, counter) + ": " + experimentName);
						
			    		evaluations.putAll(classifyExperiment(classifier, fullTrain, fullTest, experimentList, experimentName, 
			    				classifications, ranking, domainNamesTest));
			    		//evaluations.put(experimentName,predictions.get(0));
			    		//if (ranking) evaluations.put(experimentName + "_ranking",predictions.get(1));
			    		
					} catch (Exception e) {
						System.err.println("[WekaBuilder.classify]: Error Classifying " + experimentName + ", " + e);
			    		e.printStackTrace();
					}
				}
			}
			//allclassifications.add(classifications);
			return evaluations;
    	} catch (Exception e) {
    		System.err.println("[WekaBuilder.classify]: " + e);
    		e.printStackTrace();
    	}
    	return null;
	}
	
	private HashMap<String,ArrayList<Double>> classifyExperiment(Classifier classifier, Instances fullTrain, HashMap<String,Instances> fullTest, HashSet<String> experimentList, 
			String experimentName, HashMap<String,double [][]> classifications, boolean rank, ArrayList<String> testingGenres) throws Exception {
		
		HashMap<String,ArrayList<Double>> results = new HashMap<String,ArrayList<Double>>();
		
		// Let's save time! We have loaded everything, so now lets run on different sets of features to find out which are best.
		Instances train = subset(fullTrain,experimentList,_exact);
		train = pseudoBalance(train);
		train.setClass(train.attribute("influencer"));
		classifier.buildClassifier(train);
		
		Evaluation eval = new Evaluation(train);
		eval.setDiscardPredictions(true);
		
		for (String key : fullTest.keySet()) {
			ArrayList<ArrayList<Double>> r = classifyExperimentTest(classifier, eval, fullTest.get(key), 
					experimentList, key + "_" + experimentName, classifications, rank);
			results.put(key + "_" +   experimentName, r.get(0));
			results.put(key  + "_" + experimentName + "_ranking", r.get(1));
		}
		return results;
	}
	
	private ArrayList<ArrayList<Double>> classifyExperimentTest(Classifier classifier, Evaluation eval, Instances fullTest, 
			HashSet<String> experimentList, String experimentName, HashMap<String,double [][]> classifications, boolean rank) throws Exception {
		
		ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();
		
		Instances test = subset(fullTest,experimentList,_exact);
		double [] p = eval.evaluateModel(classifier,test);
		//FastVector evaluations = eval.predictions();
		
		HashMap<Instance,Integer> positions = new HashMap<Instance,Integer>();
		
		for (int index = 0; index < test.numInstances(); index++) {
			positions.put(test.instance(index), index);
		}

		ArrayList<Double> predictions = new ArrayList<Double>();
		for (int i = 0; i < p.length; i++) {
			predictions.add(p[i]);
		}
		results.add(predictions);
		classifications.put(experimentName, eval.confusionMatrix());
		
		if (rank) {
			HashMap<String,ArrayList<Instance>> ranking = new HashMap<String,ArrayList<Instance>>();
			
			// details
			for (int i = 0; i < test.numInstances(); i++) {
				ArrayList<Instance> doc = ranking.containsKey(String.valueOf(fullTest.instance(i).value(fullTest.attribute("qid")))) ?
						ranking.get(String.valueOf(fullTest.instance(i).value(fullTest.attribute("qid")))) : new ArrayList<Instance>();
				doc.add(test.instance(i));
				ranking.put(String.valueOf(fullTest.instance(i).value(fullTest.attribute("qid"))), doc);
			}
			ArrayList<Double> rankpredictions = new ArrayList<Double>();
			while (rankpredictions.size() < test.numInstances()) rankpredictions.add(0.0);
			
			classifications.put(experimentName + "_ranking", classifyRanking(classifier,test,ranking, rankpredictions, positions));
			results.add(rankpredictions);
		}
		return results;
	}
	
	protected double [][] classifyRanking(final Classifier classifier, Instances data, HashMap<String,ArrayList<Instance>> ranking, 
			ArrayList<Double> newScores, HashMap<Instance, Integer> positions) {
		double [][] confusionMatrix = {{0,0},{0,0}};
		
		class InfluenceComparator implements Comparator<Instance> {
			@Override
			public int compare(Instance i1, Instance i2) {
				try {
					Double score1 = classifier.distributionForInstance(i1)[1];
					Double score2 = classifier.distributionForInstance(i2)[1];

					return score2.compareTo(score1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return -1;
			}
		}
		
		//int docCount = 0;
		
		for (String doc : ranking.keySet()) {
			ArrayList<Instance> instances = ranking.get(doc);
			/*HashMap<Instance,Integer> position  = new HashMap<Instance,Integer>();
			
			for (int index = 0; index < instances.size(); index++) {
				position.put(instances.get(index), index);
			}*/
			
			Collections.sort(instances, new InfluenceComparator());
			
			int i = 0;
			
			for (Instance instance : instances) {
				
				try {
					double influence = classifier.distributionForInstance(instance)[1];
					double value = classifier.classifyInstance(instance);
					// person with highest confidence, and anyone with confidence > 0 is an influencer
					if (influence > .8 || i == 0 || value == 1) {
						if (instance.stringValue(data.attribute("influencer")).equals("Y")) {
							confusionMatrix[1][1]++; //TP
						}
						else if (instance.stringValue(data.attribute("influencer")).equals("N")) {
							confusionMatrix[0][1]++; //FP
							if (Influencer.DEBUG) System.out.println(doc + ":" + influence + ":" + instance.stringValue(data.attribute("influencer")) + ":" + instance.toString());
						}
						newScores.set(positions.get(instance),1.0);
					}
					else {
						if (instance.stringValue(data.attribute("influencer")).equals("Y")) {
							confusionMatrix[1][0]++; //FN
							if (Influencer.DEBUG) System.out.println(doc + ":" + influence + ":" + instance.stringValue(data.attribute("influencer")) + ":" + instance.toString());
						}
						else if (instance.stringValue(data.attribute("influencer")).equals("N")) {
							confusionMatrix[0][0]++; //TN
						}
						newScores.set(positions.get(instance),0.0);
					}
					i++;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return confusionMatrix;
	}
	
	protected void computeTestNGram(Instances train, Instances test, String type) {
		
		for (int i = 0; i < train.numAttributes(); i++) {
			if (train.attribute(i).name().startsWith(type)) {
				test.insertAttributeAt(train.attribute(i), i);
			}
		}
	}
	
	protected List<Object> computeNGrams(Instances train, HashSet<String> ngrams, StringToWordVector filter, 
			String type, boolean adjustNames, ArrayList<String> domainNames) {

		List<Object> modified = new ArrayList<Object>();
		
		try {
			// train
			train = ChiSquareFeatureSelection.applyNonWekaFilter(train, new int[] {train.attribute(type).index()}, type, 
					1, 3, 100, true, "influencer", domainNames);
		    modified = ChiSquareFeatureSelection.applyFilter(train, type, "influencer", domainNames,true);
		    return modified;
			
		} catch (Exception e) {
			System.err.println("[WekaBuilder.computeNGrams] " + e);
			e.printStackTrace();
		}
		
		return modified;
	}
	
	protected HashMap<String,Instances> computeNGramsTest(HashMap<String,Instances> test, HashSet<String> ngrams, 
			String type) {

		HashMap<String,Instances> modified = new HashMap<String,Instances>();
		
		try {
			for (String key : test.keySet())
					modified.put(key,ChiSquareFeatureSelection.applyNonWekaTestFilter(
							test.get(key), ngrams, type, "influencer", 1, 3));
			return modified;
		} catch (Exception e) {
			System.err.println("[WekaBuilder.computeNGrams] " + e);
			e.printStackTrace();
		}
		
		return modified;
	}
	
	/*private StringToWordVector generateNgramFilt() {
		//Stemmer stem = new SnowballStemmer();
		
		StringToWordVector ngramFilt = new StringToWordVector();
        //ngramFilt.setStemmer(stem);
        ngramFilt.setLowerCaseTokens(true);
        ngramFilt.setWordsToKeep(100);
        ngramFilt.setOutputWordCounts(true);        
        ngramFilt.setDoNotOperateOnPerClassBasis(true);
        ngramFilt.setUseStoplist(true);
        return ngramFilt;
	}*/

	
	private Instances subset(Instances instances, HashSet<String> experiments, boolean exact) {
		
		//System.out.print(Arrays.toString(experiments.toArray()) + " --> ");
		
		Instances subset = new Instances(instances);
		String remove = "";
		/*boolean age = false;
		boolean religion = false;
		boolean gender = false;
		boolean politics = false;
		
		boolean majority = false;*/
		
		/*if (_topics) {
			for (String e : experiments) {
				if (e.indexOf("age") >= 0 || (!exact && e.indexOf("majority") >= 0)) age = true;
				if (e.indexOf("religion") >= 0 ||  (!exact && e.indexOf("majority") >= 0)) religion = true;
				if (e.indexOf("gender") >= 0 ||  (!exact && e.indexOf("majority") >= 0)) gender = true;
				if (e.indexOf("political") >= 0 ||  (!exact && e.indexOf("majority") >= 0)) politics = true;
				
				if (e.indexOf("majority") >= 0) majority = true;
			}
		}*/
		
		try {
			
			for (int index = 0; index < subset.numAttributes(); index++) {
				if (subset.attribute(index).name().equals("influencer")) continue; // || subset.attribute(index).name().equals("qid")) continue;
				if (subset.attribute(index).name().equals("num_words")) continue;
				if (!experiments.isEmpty() && subset.attribute(index).name().indexOf("_topic") >= 0) continue;
				/*if (_topics && majority && subset.attribute(index).name().indexOf("gender_topic") >= 0 && gender) continue;
				if (_topics && majority && subset.attribute(index).name().indexOf("religion_topic") >= 0 && religion) continue;
				if (_topics && majority && subset.attribute(index).name().indexOf("politics_topic") >= 0 && politics) continue;
				if (_topics && majority && subset.attribute(index).name().indexOf("age_topic") >= 0 && age) continue;*/
				boolean found = false;
				
				for (Object experiment : experiments.toArray()) {
					if (((String)experiment).isEmpty()) continue;
					if (subset.attribute(index).name().indexOf((String)experiment) >= 0) {
					    //System.err.print(subset.attribute(index) + ": " + eval.evaluateAttribute(index) + ", ");
						found = true;
						break; 
					}
				}
				if (!found) {
					//subset.deleteAttributeAt(index);
					//index--;
					remove += (index+1) + ",";
				}
				/*else {
					System.out.print(subset.attribute(index).name() + ", ");
				}*/
			}
			subset = InstancesProcessor.removeList(subset, remove);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("");
		return subset;
	}
	
	
	public static Classifier getClassifier(String classifier)  {
		if (classifier.equals("nb")) return new NaiveBayes();
		if (classifier.equals("j48")) return new J48();
		//if (classifier.equals("id3")) return new Id3();
		if (classifier.equals("svm")) {
			SMO svm =  new SMO();
			svm.setC(10^1);
			svm.setBuildLogisticModels(true);
			return svm;
		}
		if (classifier.equals("logistic")) return new Logistic();
		return null;
	}
	
	private Instances pseudoBalance(Instances instances) {
		
		int classPosition = instances.numAttributes()-1;
		AttributeStats stats = instances.attributeStats(classPosition);
		int nCounts = stats.nominalCounts[0];
		int yCounts = stats.nominalCounts[1];
		
		for (int i = 0; i < instances.numInstances(); i++) {
			double yFactor = (double)nCounts / yCounts;
		
			if (instances.instance(i).stringValue(classPosition).equals("Y")) {
				instances.instance(i).setWeight(yFactor);
			}
		}
		return instances;
	}
	
	public void computeCorrelation(Instances instances, boolean binned) {
		
		double [][] data = new double[instances.numInstances()][instances.numAttributes()];
		
		HashMap<String,HashMap<String,Integer>> counts = new HashMap<String,HashMap<String,Integer>>();
		
		for (int j = 0; j < instances.numAttributes(); j++) {
			System.out.print(instances.attribute(j).name() + ",");
			counts.put(instances.attribute(j).name(), new HashMap<String,Integer>());
		}
		System.out.println("");
		
		for (int i = 0; i < instances.numInstances(); i++) {
			for (int j = 0; j < instances.numAttributes(); j++) {
				data[i][j] = instances.instance(i).value(j);
				if (binned) 
					counts.get(instances.attribute(j).name()).put(
							String.valueOf(data[i][j]),counts.get(instances.attribute(j).name()).containsKey(String.valueOf(data[i][j])) ? counts.get(instances.attribute(j).name()).get(String.valueOf(data[i][j]))+1 : 1);
				else System.out.print(instances.instance(i).value(j) + ",");
			}
			if (!binned) System.out.println("");
		}
		SpearmansCorrelation correlation = new SpearmansCorrelation();
		RealMatrix values = correlation.computeCorrelationMatrix(data);
		if (binned) {
			for (String attribute : counts.keySet()) {
				System.out.println(attribute);
				for (String value : counts.get(attribute).keySet()) {
					System.out.println(value + "," + counts.get(attribute).get(value));
				}
				System.out.println("");
			}
		}
		System.out.println(Arrays.toString(values.getData()[instances.classAttribute().index()]));
		System.out.println(Arrays.toString(new SpearmansCorrelation(values).getRankCorrelation().getCorrelationPValues().getData()[instances.classAttribute().index()]));
	}
}
