/**
 * 
 */
package utils;

/**
 * @author sara
 *
 */
public class Annotation {

	String _source;
	String _file;
	String _author;
	String _confidence;
	String _annotator;
	
	public Annotation(String source, String file, String author, String confidence, String annotator) {
		_source = source.trim();
		_file = file.trim();
		_author = author.trim();
		_confidence = confidence.trim();
		_annotator = annotator.trim();
	}
	
	public String getSource() {
		return _source;
	}
	
	public String getFile() {
		return _file;
	}
	
	public String getAuthor() {
		return _author;
	}
	
	public String getConfidence() {
		return _confidence;
	}
	
	public String getAnnotator() {
		return _annotator;
	}
}
