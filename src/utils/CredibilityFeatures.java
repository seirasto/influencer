/**
 * 
 */
package utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import modules.AbstractModule;

import processing.GeneralUtils;

import blog.Blog;
import blog.Sentence;
import blog.threaded.Comment;
import blog.threaded.livejournal.Entry;
import social_media.Features;

/**
 * @author sara
 *
 */
public class CredibilityFeatures {
	
	double _mentions;
	double _mentioned;
	int _titles;
	double _quotes;
	double _numbers;
	String _text;
	Hashtable<String,Double> _features;
	int _numPosts;
	boolean _mostQuestions;
	double _inquisitiveness;
	double _postInquisitiveness;
	String [] _honorifics = {"Ms","Mrs","Mr","Master","Rev","Reverend","Dr","Doctor","Atty","Prof","Attorney","Professor",
			"Honorable","President","Pres","Gov","Governer","Coach","Officer","Monsignor","Superintendent","Senator",
			"Representative","Ambassador","Treasurer","Secretary","Corporal","Sargent","Administrative","Captain",
			"Commander","Lieutenant","Colonel","General","Sir","Madam","PhD","Queen","King","Prince","Princess",};

	
	public CredibilityFeatures(Blog blog, String author, Set<String> authors, ArrayList<String> posts) {
		Properties prop = GeneralUtils.getProperties("config/config.properties");
		
		Features smFeatures = new Features(prop.getProperty("dictionary"),
				prop.getProperty("emoticons_directory") + "/emoticons.txt",prop.getProperty("emoticons_directory") + "/acronyms.txt");
		_text = AbstractModule.getText((blog.threaded.livejournal.Blog) blog,posts);
		_features = smFeatures.compute(_text.split("\\s+"));
		computeHonorificsAndNameMentions(_text, blog, posts, authors, author);
		_quotes = _features.get(Features.QUOTES);
		_numPosts = posts.size();
		_mostQuestions = false;
		_inquisitiveness = ((double) computeInquisitveness((Entry) blog.getEntry(blog.getEntries().next()), posts));
		_postInquisitiveness = ((double) computePostInquisitveness((Entry) blog.getEntry(blog.getEntries().next()), posts));

	}
	
	public double getMisspellings() {
		return _features.get(Features.SLANG);
	}
	
	public double getLinks() {
		return _features.get(Features.LINKS);
	}
	
	/**
	 * How many times this author mentions other people
	 * @return
	 */
	public double getMentions() {
		return _mentions;
	}

	/**
	 * How many times this author is mentioned by other people
	 * @return
	 */
	public double getMentioned() {
		return _mentioned;
	}	
	
	public int getHonorifics() {
		return _titles;
	}
	
	public double getQuotes() {
		return _quotes;
	}
	
	public double getNumbers() {
		return _numbers;
	}
	
	public void setMostQuestions() {
		_mostQuestions = true;
	}
	
	public boolean hasMostQuestions() {
		return _mostQuestions;
	}
	
	public int numPosts() {
		return _numPosts;
	}
	
	public String toString() {
		return "mentions: " + _mentions + ", honorifics: " + _titles + ", quotes: " + _quotes + ", links: " + getLinks() + ", misspellings: " + getMisspellings();
	}
	
	private void computeHonorificsAndNameMentions(String text, Blog blog, ArrayList<String> posts, Set<String> authors, String author) {
		
		int count = 0;
		text = text.toLowerCase();
		
		for (String a : authors) {
			Pattern p = Pattern.compile("\\Q" + a.toLowerCase() + "\\E");
			Matcher m = p.matcher(text);

			while (m.find()) {
				count++;
			}
		}
		_mentions = (double)count;
		
		count = 0;
		Entry e = (Entry) blog.getEntry(blog.getEntries().next());
		String otherText = "";
		if (!blog.getUsername().equalsIgnoreCase(author)) otherText += e.getEntryText() + " ";
		
		for (String url : e.getCommentKeys()) {
			Comment c = (Comment) e.getComment(url);
			
			if (!c.getUsername().equalsIgnoreCase(author)) otherText += c.getCommentText() + " ";
		}
			
		Pattern p = Pattern.compile("\\Q" + author.toLowerCase() + "\\E");
		Matcher m = p.matcher(otherText.toLowerCase());

		while (m.find()) {
			count++;
		}
		_mentioned = (double)count;
		
		count = 0;
		String titles = Arrays.toString(_honorifics);
		p = Pattern.compile("\\b(" + titles.substring(1,titles.length()-1).replaceAll(",", "|").toLowerCase() + ")\\b");
		m = p.matcher(text);
		while (m.find()) {
			count++;
		}
		_titles = count;
		
		count = 0;
		p = Pattern.compile("\\b[0-9,\\.-]+\\b");
		m = p.matcher(text);
		while (m.find()) {
			count++;
		}
		_numbers = count;
	}

	public double inquisitiveness() {
		return _inquisitiveness;
	}
	
	public double postInquisitiveness() {
		return _postInquisitiveness;
	}

	/**
	 * Question mark at end of sentence.
	 * Acceptable: ? ?!? ?-- etc..
	 * @param entry
	 * @param posts
	 * @return
	 */
	public static int computeInquisitveness(Entry entry, ArrayList<String> posts) {
		
		int inquisitiveness = 0;
		
		Pattern p = Pattern.compile("\\p{Punct}+$");
		 
		for (String id : posts) {
			List<Sentence> sentences = null;
			
			if (id.equals("-1")) sentences = entry.getSentences();
			else sentences = entry.getComment(id).getSentences();
			
			for (Sentence sentence : sentences) {
				 Matcher m = p.matcher(sentence.getText().trim());
				 String end = "";
				 if (m.find()) end = m.group();
				 if (end.contains("?")) inquisitiveness++;
			}
		}
		
		return inquisitiveness;
	}
	
	public static int computePostInquisitveness(Entry entry, ArrayList<String> posts) {
		
		int inquisitiveness = 0;
		
		Pattern p = Pattern.compile("\\p{Punct}+$");
		 
		for (String id : posts) {
			List<Sentence> sentences = null;
			
			if (id.equals("-1")) sentences = entry.getSentences();
			else sentences = entry.getComment(id).getSentences();
			
			for (Sentence sentence : sentences) {
				 Matcher m = p.matcher(sentence.getText().trim());
				 String end = "";
				 if (m.find()) end = m.group();
				 if (end.contains("?")) inquisitiveness++;
				 break;
			}
		}
		
		return inquisitiveness;
	}
}
