/**
 * 
 */
package utils;

import blog.*;
import blog.threaded.livejournal.Entry;
import blog.threaded.livejournal.Comment;

import java.util.*;

/**
 * @author sara
 *
 */
public class DialogFeatures {

	String _author;
	ArrayList<String> _posts;
	Blog _blog;
	Entry _entry;
	/*int _incitation;
	int _inquisitiveness;
	int _investment;*/
	boolean _initiative;
	boolean _last;
	boolean _max;
	boolean _longestPost;
	//int _numSentences; // for normalization
	/*boolean _consecutive;
	int _irrelevance;
	int _interjection;
	boolean _alternatingPost;
	int _numSentences; // for normalization
	long _activeTime;*/
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public DialogFeatures(Blog blog, String author, ArrayList<String> posts) {
		
		_posts = posts;
		_author = author;
		_blog = blog;
		_max = false;
		_longestPost = false;
		_entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		//_numSentences = DialogFeatures.computeNumSentences(_entry, _posts);
		_longestPost = false;
	}
	
	public void setMax() {
		_max = true;
	}
	
	public void setLongestPost() {
		_longestPost = true;
	}
	
	public String getAuthor() {
		return _author;
	}
	
	public boolean hasInitiative() {
		return _author.equals(_blog.getUsername()) ? true : false;
	}
	
	public boolean isLast() {
		return _author.equals(((Comment)_entry.getSortedComments()[_entry.getNumComments()-1]).getUsername())
				? true : false;
	}
	
	public boolean hasConsecutive() {
		return DialogFeatures.computeConsecutive(_blog, _author);
	}
	
	public boolean isMax() {
		return _max;
	}
	
	public boolean hasLongestPost() {
		return _longestPost;
	}
	
	public boolean hasAlternating() {
		return DialogFeatures.computeAlternation(_blog, _author);
	}
		
	public int incitation() {
		return DialogFeatures.computeIncitation(_blog, _author);
	}
	
	public int investment() {
		return _posts.size();
	}
	
	public double irrelevance() {
		return DialogFeatures.computeIrrelevance(_entry, _posts)/(double)investment();
	}
	
	public int interjection() {
		return DialogFeatures.computeInterjection(_blog, _author);
	}
	
	public long getActiveTime() {
		return DialogFeatures.computeActiveTime(_blog, _author);
	}
	
	public double getResponseTime() {
		return DialogFeatures.computeResponseTime(_blog, _author);
	}
	
	/*public int numSentences() {
		return _numSentences;
	}*/
	
	public int numPosts() {
		return _posts.size();
	}
	
	public static int numSentences(Blog blog, String author) {
		int numSentences = 0;
		
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		if (blog.getUsername().equals(author)) numSentences += entry.getSentences().size();
		
		Object [] comments = entry.getSortedComments();
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			
			if (c.getUsername().equals(author)) numSentences += c.getSentences().size();
		}
		return numSentences;	
	}
	
	public static int numWords(Blog blog, String author) {
		int numWords = 0;
		
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		if (blog.getUsername().equals(author)) numWords += entry.getEntryText().split("\\s+").length;
		
		Object [] comments = entry.getSortedComments();
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			
			if (c.getUsername().equals(author)) numWords += c.getCommentText().split("\\s+").length;
		}
		return numWords;
	}

	public static int computeInterjection(Blog blog, String author) {
		
		if (blog.getUsername().equals(author)) return 0;
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		Object [] comments = entry.getSortedComments();
		
		int count = 1;
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			
			if (c.getUsername().equals(author)) return count;
			count++;
		}
		return -1;
	}
	
	public static int computeIrrelevance(Entry entry, List<String> posts) {
		
		Object [] comments = entry.getSortedComments();
		
		HashSet<String> ids = new HashSet<String>();
		
		for (String post : posts) {
			ids.add(post);
		}
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			
			if (ids.contains(c.getParentURL())) ids.remove(c.getParentURL());
		}
		return ids.size();
	}
	
	public static boolean computeConsecutive(Blog blog, String author) {
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		Object [] comments = entry.getSortedComments();
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			if (!c.getUsername().equals(author)) continue;

			String parent = "";
			
			if (c.getParentURL() == null || c.getParentURL().equals("-1") || entry.getComment(c.getParentURL()) == null) {
				parent = blog.getUsername();
			}
			else {
				parent = ((blog.threaded.livejournal.Comment)entry.getComment(c.getParentURL())).getUsername();
			}
			if (parent.equals(author)) return true;
		}
		return false;
	}
	
	public static boolean computeAlternation(Blog blog, String author) {
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		Object [] comments = entry.getSortedComments();
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			if (!c.getUsername().equals(author)) continue;

			String parent = (c.getParentURL() == null || c.getParentURL().equals("-1") || entry.getComment(c.getParentURL()) == null) ? blog.getUsername() : 
				((blog.threaded.livejournal.Comment)entry.getComment(c.getParentURL())).getUsername();
			if (parent.equals(author) || c.getParentURL() == null || c.getParentURL().equals("-1") || entry.getComment(c.getParentURL()) == null) continue;
			Comment p = ((blog.threaded.livejournal.Comment)entry.getComment(c.getParentURL()));
			String grandparent = (p.getParentURL() == null || p.getParentURL().equals("-1") || entry.getComment(p.getParentURL()) == null) ? blog.getUsername() : 
				((blog.threaded.livejournal.Comment)entry.getComment(p.getParentURL())).getUsername();
			if (c.getUsername().equals(grandparent)) return true;
		}
		return false;
	}
	
	public static int computeIncitation(Blog blog, String author) {

		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		Object [] comments = entry.getSortedComments();
		
		if (blog.getUsername().equals(author)) return comments.length;
		
		int incitation = 0;
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			
			int length = 0;
			
			while (c != null && c.getParentURL() != null && !c.getParentURL().equals("-1")) {
				if (c.getUsername().equals(author) && length > incitation) 
					incitation = length;
				length++;
				c = (blog.threaded.livejournal.Comment)entry.getComment(c.getParentURL());
			}
		}
		return incitation;
	}
	
	public static long computeActiveTime(Blog blog, String author) {
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		Object [] comments = entry.getSortedComments();
		
		Date first = null;
		Date last = null;
		
		if (blog.getUsername().equals(author)) {
			first = entry.getDate();
			last = entry.getDate();
		}
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			
			if (c.getUsername().equals(author)) {
				try {
					if (first == null || (c.getDate() != null && first.compareTo(c.getDate()) > 0)) first = c.getDate();
					if (last == null || (c.getDate() != null && last.compareTo(c.getDate()) < 0)) last = c.getDate();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		if (first == null || last == null) return 0;
		long difference = last.getTime() - first.getTime();
		if (difference == 0) return 0;
		return (difference / 60000);
	}
	
	public static double computeResponseTime(Blog blog, String author) {
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		Object [] comments = entry.getSortedComments();
		
		long time = 0;
		double numComments = 0;
		
		for (Object comment : comments) {
			
			Comment c = (Comment)comment;
			if (c.getDate() == null) continue;
			
			if (c.getUsername().equals(author)) {
				if (entry.getComment(c.getParentURL()) == null) {
					if (entry.getDate() == null) continue;
					time += (c.getDate().getTime() - entry.getDate().getTime());
					numComments++;
				}
				else {
					if (((Comment)entry.getComment(c.getParentURL())).getDate() == null) continue;
					time += (c.getDate().getTime() - ((Comment)entry.getComment(c.getParentURL())).getDate().getTime());
					numComments++;
				}
			}
		}
		
		return time == 0 ? 0 : (time / 60000) / numComments;
	}
	
	public static int longestPost(ArrayList<String> posts, Entry entry) {
		int max = 0;
		for (String post : posts) {
			String text = entry.getComment(post) != null ? entry.getComment(post).getCommentText() : entry.getEntryText();
			if (text.length() > max) max = text.length();
		}
		return max;
	}
	
	public String toString() {
		return _author + ": <initiative," + hasInitiative() + "> <incitation," + incitation() + "> <investment," + 
				investment() + "> <irrlevance," + irrelevance() + "> <interjection," + interjection() + "> <last " + isLast() + ">";
	}
}
