/**
 * 
 */
package utils;

import java.util.HashMap;

/**
 * @author sara
 *
 */
public class DocumentDemographics {

	HashMap<String,DemographicFeatures> _participants;
	double _yearTotal;
	int [] _gender = {0,0};
	int [] _religion = {0,0,0,0};
	int [] _politicalParty = {0,0};
	int [] _politicalView = {0,0};
	int [] _age = {0,0};
	
	public DocumentDemographics() {
		_participants = new HashMap<String,DemographicFeatures>();
	}
	
	public int [] getReligion() {
		return _religion;
	}
	
	public int [] getPoliticalParty() {
		return _politicalParty;
	}
	
	public int [] getPoliticalView() {
		return _politicalView;
	}
	
	public int [] getGender() {
		return _gender;
	}
	
	public int [] getAge() {
		return _age;
	}
	
	public void addParticipant(String name, DemographicFeatures f) {
		_participants.put(name, f);
		_gender[(int)f.getGender()]++;
		_yearTotal += f.getYear();
		_politicalParty[(int)f.getPoliticalParty()]++;
		_politicalView[(int)f.getPoliticalView()]++;
		_religion[(int)f.getReligion()]++;
		_age[(int)f.getBinaryAge()]++;
	}
	
	public HashMap<String,DemographicFeatures> getParticipants() {
		return _participants;
	}
	
	public DemographicFeatures getParticipant(String author) {
		if (!_participants.containsKey(author)) return null;
		return _participants.get(author);
	}
	
	public double getAverageYear() {
		return (_yearTotal / (double)_participants.size());
	}
	
	public double getGenderPercent(double gender) {
		return _gender[(int)gender] / (double)_participants.size();
		
	}
	
	public int getMajorityCombo(int [] type1, double value1, int [] type2, double value2) {		
		int majority1 = getMajority(type1, value1);
		int majority2 = getMajority(type2, value2);
		
		if (majority1 + majority2 == 2) return 1;
		return 0;	
	}
	
	public int getMajority(int [] type, double value) {
		int max = type[0];
		int index = 0;
		
		for (int i = 1; i < type.length; i++) {
			if (type[i] > max || (type[i] == max && i == value)) {
				max = type[i];
				index = i;
			}
		}
		
		if (value == -1) return index;
		if ((int)value == index) return 1;
		return 0;	
	}
}
