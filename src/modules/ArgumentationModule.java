/**
 * 
 */
package modules;

import java.io.File;
import java.util.*;

import blog.Sentence;
import blog.threaded.livejournal.Blog;

//import persuasion.app.*;
import persuasion.app.pipeline.ArgumentationRunner;
import processing.GeneralUtils;
import utils.Author;
import weka.core.Attribute;
import weka.core.Instance;

/**
 * @author sara
 *
 */
public class ArgumentationModule extends AbstractModule {

	String _outputDirectory = null;
	String _dataDirectory = null;
	String _annotationDirectory = null;
	String _model = null;
	
	/**
	 * 
	 */
	public ArgumentationModule() {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			_outputDirectory =  prop.getProperty("output");
			_dataDirectory =  prop.getProperty("data");
			_model = prop.getProperty("argumentation_model");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#run(java.lang.String)
	 */
	@Override
	public void run(String file) {
		run(file,"");
	}
	
	public void run(String file, String type) {

		// model $home/LUs/Argument/argument/data/lj/all-annotated-entries-flat2
		String [] args = ("-output " + _outputDirectory + "/processed/" + type + "/" + " -unseen " + file  + 
				" -train " + _model + " -classifier naivebayes").split(" ");
		ArgumentationRunner.main(args);
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#read(java.lang.String)
	 */
	@Override
	public List<Object> read(String input) {
		// TODO Auto-generated method stub
		return ArgumentationRunner.read(input);
	}

	public HashMap<String,ArrayList<Object>> readByPost(String input) {
		// TODO Auto-generated method stub
		try {
			return ArgumentationRunner.readByPost(input); //,_processingDirectory, _committedBelief);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("[ArgumentationModule.readByPost] Error on: " + input);
			return null;
		}
	}
	
	private static Attribute _argumentationHas, _argumentationCountArgumentation, _argumentationCountSentences,  _numArgumentationPosts, _mostArgumentationPost, 
		/*_mostArgumentative,*/ _text, _firstArgumentation;
	
	public static ArrayList<String> getExperiments(boolean exact) {
		ArrayList<String> experiments = new ArrayList<String>(); 
		
		if (!exact) {
			experiments.add("argumentation_");
			return experiments;
		}
		
		experiments.add("argumentation_has");
		experiments.add("argumentation_count_argumentation");
		experiments.add("argumentation_count_sentences");
		experiments.add("argumentation_numPosts");
		//experiments.add("argumentation_most");
		experiments.add("argumentation_mostPost");
		experiments.add("argumentation_text");
		experiments.add("argumentation_first");
		return experiments;
	}
	
	public static void buildAttributes(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges, ArrayList<String> featureNames) {
	
		_argumentationHas = new Attribute("argumentation_has");
		_argumentationCountSentences = new Attribute("argumentation_count_sentences");
		_argumentationCountArgumentation = new Attribute("argumentation_count_argumentation");
		_numArgumentationPosts = new Attribute("argumentation_numPosts");
		_mostArgumentationPost = new Attribute("argumentation_mostPost");
		//_mostArgumentative = new Attribute("argumentation_most");
		_firstArgumentation = new Attribute("argumentation_first");
		_text = new Attribute("argumentation_text",(ArrayList<String>)null);
		addAtt(attributes, featureRanges, featureNames, _argumentationHas, "argumentation_has");
		addAtt(attributes, featureRanges, featureNames, _argumentationCountArgumentation, "argumentation_count_argumentation");
		addAtt(attributes, featureRanges, featureNames, _argumentationCountSentences, "argumentation_count_sentences");
		addAtt(attributes, featureRanges, featureNames, _numArgumentationPosts, "argumentation_numPosts");
		addAtt(attributes, featureRanges, featureNames, _mostArgumentationPost, "argumentation_mostPost");
		//addAtt(attributes, featureRanges, featureNames, _mostArgumentative, "argumentation_most");
		addAtt(attributes, featureRanges, featureNames, _text, "argumentation_text");
		addAtt(attributes, featureRanges, featureNames, _firstArgumentation, "argumentation_first");
	}
	
	public static void setInstance(Instance instance, HashMap<String,ArrayList<Object>> argumentations, 
			Author author, Blog blog, double numArgumentation, double numPosts, double numSentences) {
		
		double argumentationCount = 0;
		double numArgumentativePosts = 0;
		double first = 0;
		double most = 0;
		String content = "";
		if (argumentations != null) {
			for (String url : author.getPosts()) {
				
				if (argumentations.get(url) == null || argumentations.get(url).size() == 0) continue;
				
				List<Sentence> text = getPost(blog, url);
				argumentationCount += argumentations.get(url).size();
				numArgumentativePosts++;
				if (text.size() > 0 && argumentations.containsKey(url) && argumentations.get(url).size() > 0
						&& argumentations.get(url).get(0).equals(text.get(0).getText())) first++;
				if (argumentations.get(url).size() > 0 && text.size() > 0 && (argumentations.get(url).size()/(double)text.size()) > most) most = argumentations.get(url).size()/(double)text.size();
				
				for (Object sentence : argumentations.get(url)) {
					content += sentence + " ";
				}
	
			}
		}
		instance.setValue(_firstArgumentation,first/author.getPosts().size());
		instance.setValue(_argumentationCountArgumentation,argumentationCount > 0 ? argumentationCount/numArgumentation : 0);
		instance.setValue(_argumentationCountSentences,argumentationCount > 0 ? argumentationCount/numSentences : 0);
		instance.setValue(_argumentationHas,argumentationCount > 0 ? 1 : 0);
		instance.setValue(_numArgumentationPosts,numArgumentativePosts/author.getPosts().size());
		instance.setValue(_mostArgumentationPost,most);
		//instance.setValue(_mostArgumentative,0);
		instance.setValue(_text,content);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/Armenian_Genocide__Archive_19___Rewriting_parts_of_this_article.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Israel_Defense_Forces__Archive_2___Image_gallery_edits.xml";
		String file = "/proj/nlp/fluke/corpora/politics/create-debate/republican-primary/debate.show.Should_the_National_Parks_Service_be_abolished_lj.xml";
		file = "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/13123.xml";
		ArgumentationModule module = new ArgumentationModule();
		module.run(file);
		HashMap<String,ArrayList<Object>> argumentation = module.readByPost("/proj/nlp/users/sara/influence/processed/livejournal/processed-" + 
		new File(file).getName() + "/" + new File(file).getName()  + ".argument");
		
		for (String key : argumentation.keySet()) {
			System.out.println(key + ". " + Arrays.toString(argumentation.get(key).toArray()));
		}
	}

}
