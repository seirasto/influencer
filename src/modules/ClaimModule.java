/**
 * 
 */
package modules;

import java.io.*;
import java.util.*;

import blog.Sentence;
import blog.threaded.livejournal.Blog;

import processing.GeneralUtils;
import utils.Author;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import claim.*;

/**
 * @author sara
 *
 */
public class ClaimModule extends AbstractModule {


	String _dataDirectory = null;
	String _processingDirectory = null;
	String _modelDirectory = null;
	String _trainingFile = null;
	String _opinionModel = null;
	ArrayList<String> _trainingType = null;
	boolean _committedBelief = false;
	ClaimGenerator _claimGenerator;
	Instances _train;
	Classifier _classifier;
	
	/**
	 * 
	 */
	public ClaimModule(boolean processing) {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			_dataDirectory = prop.getProperty("claim_data");
			_processingDirectory = prop.getProperty("processing");
			_trainingFile = prop.getProperty("claim_training_files");
			_trainingType = new ArrayList<String>(Arrays.asList(prop.getProperty("claim_training_types").split(",")));
			_committedBelief = prop.getProperty("claim_committed_belief").equals( "true" ) ? true : false;
			_opinionModel = prop.getProperty("opinion_training_file");
			_modelDirectory = prop.getProperty("output") + "/models/claim/";
			if (processing) {
				_claimGenerator = new ClaimGenerator(_trainingType, _dataDirectory,
						prop.getProperty("output") , _modelDirectory, .7, 250, true,false,true,true,true,true,true,true, _opinionModel, true, false);
				_train = _claimGenerator.train(_trainingType, _dataDirectory, true);
				_classifier = _claimGenerator.buildClassifier(_train, _trainingType, "influencer", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void run(String file) {
		run(file,"");
	}
	
	public void run(String file, String type) {
		
		try {
			_claimGenerator.run(_train, _classifier, _trainingType, file,"influencer", _processingDirectory + "/" + type + "/");
		} catch (Exception e) {
			System.err.println("[ClaimModule.run] " + e);
			e.printStackTrace();
		}
	}
	
	private static Attribute _claimHas, _claimCountClaims, _claimCountSentences, _numClaimPosts, /*_mostClaims,*/ _mostClaimsPost, _first, _text;
	
	public static ArrayList<String> getExperiments(boolean exact) {
		ArrayList<String> experiments = new ArrayList<String>(); 
		
		if (!exact) {
			experiments.add("claim_");
			return experiments;
		}
		
		experiments.add("claim_has");
		experiments.add("claim_count_claims");
		experiments.add("claim_count_sentences");
		experiments.add("claim_numPosts");
		//experiments.add("claim_most");
		experiments.add("claim_mostPost");
		experiments.add("claim_first");
		experiments.add("claim_text");
		return experiments;
	}
	
	public static void buildAttributes(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges, ArrayList<String> featureNames) {
	
		_claimHas = new Attribute("claim_has");
		_claimCountClaims = new Attribute("claim_count_claims");
		_claimCountSentences = new Attribute("claim_count_sentences");
		_numClaimPosts = new Attribute("claim_numPosts");
		//_mostClaims = new Attribute("claim_most");
		_mostClaimsPost = new Attribute("claim_mostPost");
		_first = new Attribute("claim_first");
		_text = new Attribute("claim_text", (ArrayList<String>) null);
		addAtt(attributes, featureRanges, featureNames, _claimHas, "claim_has");
		addAtt(attributes, featureRanges, featureNames, _claimCountClaims, "claim_count_claims");
		addAtt(attributes, featureRanges, featureNames, _claimCountSentences, "claim_count_sentences");
		addAtt(attributes, featureRanges, featureNames, _numClaimPosts, "claim_numPosts");
		//addAtt(attributes, featureRanges, featureNames, _mostClaims, "claim_most");
		addAtt(attributes, featureRanges, featureNames, _mostClaimsPost, "claim_mostPost");
		addAtt(attributes, featureRanges, featureNames, _first, "claim_first");
		addAtt(attributes, featureRanges, featureNames, _text, "claim_text");
	}
	
	public static void setInstance(Instance instance, HashMap<String,ArrayList<Object>> claims, 
			Author author, Blog blog, double numClaims, double numPosts, double numSentences) {
				
		double claimCount = 0;
		int numClaimPosts = 0;
		double most = 0;
		double first = 0;
		String content = "";
		
		for (String url : author.getPosts()) {
			
			List<Sentence> text = getPost(blog, url);
			
			for (Object sentence : claims.get(url)) {
				content += sentence + " ";
			}
			
			claimCount += claims.get(url).size();
			if (claims.get(url).size() > 0) numClaimPosts++;
			if (text.size() > 0 && claims.containsKey(url) && claims.get(url).size() > 0 && claims.get(url).get(0).equals(text.get(0).getText())) first++;
			if (claims.get(url).size() > 0 && text.size() > 0 && claims.get(url).size()/(double)text.size() > most) most = claims.get(url).size()/(double)text.size();
		}

		instance.setValue(_claimHas, claimCount > 0 ? 1 : 0);
		instance.setValue(_claimCountSentences,claimCount > 0 ? claimCount / numSentences : 0);
		instance.setValue(_claimCountClaims,claimCount > 0 ? claimCount / numClaims : 0);
		instance.setValue(_numClaimPosts,numClaimPosts/author.getPosts().size());
		//instance.setValue(_mostClaims,most);
		instance.setValue(_mostClaimsPost,most);
		instance.setValue(_first, first / author.getPosts().size());
		instance.setValue(_text, content);
	}
	
	
	/* (non-Javadoc)
	 * @see modules.AbstractModule#read(java.lang.String)
	 * 
	 * Read in claims from XML file
	 */
	@Override
	public List<Object> read(String input) {
		// TODO Auto-generated method stub
		return ClaimReader.read(input); //,_processingDirectory, _committedBelief);
	}
	
	public HashMap<String,ArrayList<Object>> readByPost(String input) {
		// TODO Auto-generated method stub
		return ClaimReader.readByPost(input); //,_processingDirectory, _committedBelief);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/Armenian_Genocide__Archive_19___Rewriting_parts_of_this_article.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Heavy_metal_music__Archive_6___Degree_of_synonymity.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Israel_Defense_Forces__Archive_2___Image_gallery_edits.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//List_of_topics_characterized_as_pseudoscience__Archive_13___Darwinism.xml";
		
		String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads/Death__Archive_4___Images.xml";
		file = "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/13123.xml";
		
		ClaimModule module = new ClaimModule(true);
		module.run(file,"livejournal");
		HashMap<String,ArrayList<Object>> claims = module.readByPost("/proj/nlp/users/sara/influence/processed/livejournal/processed-" + 
		new File(file).getName() + "/" + new File(file).getName()  + ".claims");
		

		for (String key : claims.keySet()) {
			System.out.println(key + ". " + Arrays.toString(claims.get(key).toArray()));
		}
	}

}
