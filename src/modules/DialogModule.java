/**
 * 
 */
package modules;

import java.util.*;

import utils.DialogFeatures;
import weka.core.*;

import blog.threaded.livejournal.Blog;
import blog.threaded.livejournal.Entry;

/**
 * @author sara
 *
 */
public class DialogModule extends AbstractModule {
	
	/**
	 * 
	 */
	public DialogModule() {
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#run(java.lang.String)
	 */
	@Override
	public void run(String file) {
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#read(java.lang.String)
	 */
	@Override
	public List<Object> read(String input) {
		// TODO Auto-generated method stub
		return Arrays.asList(readByAuthor(input).values().toArray());
	}
	
	public HashMap<String,DialogFeatures> readByAuthor(String file) {
		
		HashMap<String,DialogFeatures> dfs = new HashMap<String,DialogFeatures>();
		
		Blog blog = Blog.processBlog(file);
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		HashMap<String,ArrayList<String>> authors = getAuthorPostList(entry, blog.getUsername());
		//String maxauthor = "";
		int maxposts = 0;
		String longestauthor = "";
		int longestpost = 0;
		
		for (String author : authors.keySet()) {
			
			ArrayList<String> posts = authors.get(author);
			
			if (posts.size() > maxposts) {
				//maxauthor = author;
				maxposts = posts.size();
			}
			int l = DialogFeatures.longestPost(posts,entry);
			if (l > longestpost) {
				longestpost = l;
				longestauthor = author;
			}
			
			DialogFeatures df = new DialogFeatures(blog, author, posts);
			
			dfs.put(author, df);
		}
		
		for (String author : authors.keySet()) {
			if (authors.get(author).size() == maxposts) dfs.get(author).setMax();
		}
		dfs.get(longestauthor).setLongestPost();
		return dfs;
	}
	
	private static Attribute _incitation,  _investment, _once, _initiative, _irrelevance, _interjection, 
		_last, _maxposts, _longestpost, _consecutiveposts,  _repetition,  /*_alternatingposts, */ _responsetime, _activetime;
	
	public static ArrayList<String> getExperiments(boolean exact) {
		ArrayList<String> experiments = new ArrayList<String>(); 
		
		if (!exact) {
			experiments.add("dialog_");
			return experiments;
		}
		
		experiments.add("dialog_initiative");
		experiments.add("dialog_investment");
		experiments.add("dialog_irrelevance");
		experiments.add("dialog_incitation"); // keep out
		experiments.add("dialog_interjection");
		
		// new
		experiments.add("dialog_last");
		experiments.add("dialog_maxposts");
		experiments.add("dialog_longestpost");
		experiments.add("dialog_consecutiveposts");
		//experiments.add("dialog_alternatingposts");
		experiments.add("dialog_repetition");
		experiments.add("dialog_responsetime");
		experiments.add("dialog_activetime");
		experiments.add("dialog_once");
		return experiments;
	}
	
	public static void buildAttributes(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges, ArrayList<String> featureNames) {
	
		// original
		_incitation = new Attribute("dialog_incitation"); // keep out
		_investment = new Attribute("dialog_investment");
		_initiative = new Attribute("dialog_initiative");
		_irrelevance = new Attribute("dialog_irrelevance");
		_interjection = new Attribute("dialog_interjection");
		
		addAtt(attributes, featureRanges, featureNames, _incitation, "dialog_incitation"); // keep out
		addAtt(attributes, featureRanges, featureNames, _investment, "dialog_investment");
		addAtt(attributes, featureRanges, featureNames, _initiative, "dialog_initiative");
		addAtt(attributes, featureRanges, featureNames, _irrelevance, "dialog_irrelevance");
		addAtt(attributes, featureRanges, featureNames, _interjection, "dialog_interjection");
		
		// new 
		_last = new Attribute("dialog_last");
		_maxposts = new Attribute("dialog_maxposts");
		_longestpost = new Attribute("dialog_longestpost");
		_consecutiveposts = new Attribute("dialog_consecutiveposts");
		_repetition = new Attribute("dialog_repetition");
		//_alternatingposts = new Attribute("dialog_alternatingposts");
		_responsetime = new Attribute("dialog_responsetime");
		_activetime = new Attribute("dialog_activetime");
		_once = new Attribute("dialog_once");
		
		addAtt(attributes, featureRanges, featureNames, _last, "dialog_last");
		addAtt(attributes, featureRanges, featureNames, _maxposts, "dialog_maxposts");
		addAtt(attributes, featureRanges, featureNames, _longestpost, "dialog_longestpost");
		addAtt(attributes, featureRanges, featureNames, _consecutiveposts, "dialog_consecutiveposts");
		//addAtt(attributes, featureRanges, featureNames, _alternatingposts, "dialog_alternatingposts");
		addAtt(attributes, featureRanges, featureNames, _repetition, "dialog_repetition");
		addAtt(attributes, featureRanges, featureNames, _responsetime, "dialog_responseTime");
		addAtt(attributes, featureRanges, featureNames, _activetime, "dialog_activetime");
		addAtt(attributes, featureRanges, featureNames, _once, "dialog_once");
	}

	public static void setInstance(Instance instance, DialogFeatures df, int numPosts) {
		instance.setValue(_incitation, df.incitation() / (double)numPosts); // keep out
		instance.setValue(_investment, df.investment() / (double)numPosts);
		instance.setValue(_initiative, df.hasInitiative() ? 1 : 0);
		instance.setValue(_irrelevance, df.irrelevance());
		instance.setValue(_interjection, df.interjection() / (double)numPosts);
		
		// new 
		instance.setValue(_once, df.investment() == 1 ? 1 : 0);
		instance.setValue(_last,df.isLast() ? 1 : 0);	
		instance.setValue(_maxposts,df.isMax() ? 1 : 0);
		instance.setValue(_longestpost,df.hasLongestPost() ? 1 : 0);
		instance.setValue(_consecutiveposts,df.hasConsecutive() ? 1 : 0);
		//instance.setValue(_alternatingposts, df.hasAlternating() ?  1 : 0);
		instance.setValue(_repetition, df.investment() > 1 ? 1 : 0);
		instance.setValue(_responsetime, df.getResponseTime());
		instance.setValue(_activetime, df.getActiveTime());
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/Armenian_Genocide__Archive_19___Rewriting_parts_of_this_article.xml";
		
		DialogModule module = new DialogModule();
		HashMap<String,DialogFeatures> dialogFeatures = module.readByAuthor(file);
		
		for (String key : dialogFeatures.keySet()) {
			System.out.println(key + ". " + dialogFeatures.get(key));
		}
	}
}