/**
 * 
 */
package modules;

import java.util.*;

import agreement.classifier.MalletClassifier;
import blog.Entry;
import blog.threaded.Comment;
import blog.threaded.livejournal.Blog;

import java.io.*;

import processing.GeneralUtils;

import utils.Author;
import weka.core.Attribute;
import weka.core.Instance;

import cc.mallet.classify.Classifier;
import cc.mallet.pipe.Pipe;
import cc.mallet.types.InstanceList;

/**
 * @author sara
 *
 */
public class AgreementModule extends AbstractModule {

	String _experiment = null;
	String _processingDirectory = null;
	String _outputDirectory = null;
	String _acrfType = null;
	String _testDirectory  = null;
	String _trainingDirectory = null;
	boolean _thread = false;
	boolean _pos = false;
	boolean _polarity = false;
	boolean _question = false;
	boolean _ngram = false;
	boolean _sentenceSimilarity = false;
	boolean _sm = false;
	boolean _liwc = false;
	boolean _entrainment = false;
	boolean _first = false;
	
	/**
	 * 
	 */
	public AgreementModule() {
        
        
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			_thread = prop.getProperty("agreement_thread").equals( "true" ) ? true : false;
			_pos = prop.getProperty("agreement_pos") .equals( "true" ) ? true : false;
			_polarity = prop.getProperty("agreement_polarity").equals( "true" ) ? true : false;
			_question = prop.getProperty("agreement_question").equals( "true" ) ? true : false;
			_ngram = prop.getProperty("agreement_ngram").equals( "true" ) ? true : false;
			_sentenceSimilarity = prop.getProperty("agreement_ss").equals( "true" ) ? true : false;
			_sm = prop.getProperty("agreement_sm").equals( "true" ) ? true : false;
			_liwc = prop.getProperty("agreement_liwc").equals( "true" ) ? true : false;
			_entrainment = prop.getProperty("agreement_entrainment").equals( "true" ) ? true : false;
			_first = prop.getProperty("agreement_first").equals( "true" ) ? true : false;
			_experiment = prop.getProperty("agreement_model");
			_processingDirectory = prop.getProperty("processing");
			_outputDirectory =  prop.getProperty("output");
			_acrfType =  prop.getProperty("acrf");
			_trainingDirectory = prop.getProperty("agreement_training");
			_testDirectory =  prop.getProperty("testing");				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#run()
	 */
	@Override
	public void run(String file) {
		run(file,"");
	}
	
	public void run(String file, String type) {
		try {
			MalletClassifier classifier = new MalletClassifier(null, _thread, _pos, true, true, _polarity, _question, 
	        		true, _ngram, _sentenceSimilarity, true, false, 
	        		true, -1, false, _liwc, _sm, _entrainment, _first);
			
			//String model = "maxent-thread-ngram-pos-polarity-ss-question-liwc-ss-sm-entrainment-create_debate:unannotated--1.ser.gz";
			
	        System.out.println("[AgreementModule] LOADING MODEL... ");
	     // Load classifier here 
			ObjectInputStream ois =
	            new ObjectInputStream (new FileInputStream (_outputDirectory + "/models/agreement/" + _experiment));
	        Pipe pipe = ((Classifier) ois.readObject()).getInstancePipe();
	        ois.close();
	        
		    System.out.println("[AgreementModule] LOADING TESTING... ");
	    	
	    	File f = File.createTempFile("agree", "dir");
	    	f.delete();
    		f.mkdir();
    		
		    InstanceList test = classifier.loadChain("", true, file, _processingDirectory + "/" + type + "/", f.toString(), "/proj/nlp/users/sara/influence/models/opinion/", 
		    		null, null, pipe, -1, false, false, false, "create_debate", "/proj/nlp/users/sara/influence/models/agreement/", true);
		    classifier.delete(f);

		    System.out.println("[AgreementModule] CLASSIFYING... ");
	    	classifier.printAnnotations(_outputDirectory + "/models/agreement/" + _experiment, 
	    			test,_processingDirectory + "/" + type + "/processed-" + new File(file).getName() + "/" + new File(file).getName() + ".sentences",_processingDirectory + "/" + type + "/processed-" + new File(file).getName() + "/" + new File(file).getName());
		} catch (Exception e) {
			System.err.println("Error adding agreement to " + file);
			System.err.println("[AgreementModule.run] " + e);
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#read()
	 */
	@Override
	public List<Object> read(String input) {
		// TODO Auto-generated method stub
		return Arrays.asList(agreement.io.ProcessBlog.getAnnotations(input).values().toArray());
	}
	
	public HashMap<String,String> readByPost(String input) {
		// TODO Auto-generated method stub
		return agreement.io.ProcessBlog.getAnnotations(input);
	}
	
	public static int count(Blog blog, HashMap<String,String> posts, boolean agreement) {
		
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		int count = 0;
		
		for (String key : entry.getCommentKeys()) {
			Comment comment = (Comment) entry.getComment(key);
			
			if (agreement && posts.containsKey(comment.getURL()) && posts.get(comment.getURL()).equals("agreement")) count++;
			else if (!agreement && posts.containsKey(comment.getURL()) && posts.get(comment.getURL()).equals("disagreement")) count++;
		}
		return count;
	}
	
	public static int computeTo(Author author, Blog blog, HashMap<String,String> posts, String type) {
		
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		int count = 0;
		
		for (String key : entry.getCommentKeys()) {
			Comment comment = (Comment) entry.getComment(key);
			
			if (entry.getComment(comment.getParentURL()) != null && entry.getComment(comment.getParentURL()).getUsername().equals(author.getName())) {
				if (posts.containsKey(comment.getURL()) && posts.get(comment.getURL()).equals(type)) count++;
			}
		}
		return count;
	}
	
	public static int firstTo(Author author, Blog blog, HashMap<String,String> posts, String type) {
		
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		for (Object key : entry.getSortedComments()) {
			Comment comment = (Comment)key;
			
			if (entry.getComment(comment.getParentURL()) != null && entry.getComment(comment.getParentURL()).getUsername().equals(author.getName())) {
				if (posts.containsKey(comment.getURL()) && posts.get(comment.getURL()).equals(type)) return 1;
				else return 0;
			}
		}
		return 0;
	}
	
	private static Attribute _agreementTo, _agreementFrom, _disagreementTo, _disagreementFrom, _noneFrom, _noneTo,
		_firstToAgreement, _firstFromAgreement, _firstToDisagreement, _firstFromDisagreement, _firstToNone, _firstFromNone;
	
	public static ArrayList<String> getExperiments(boolean exact) {
		ArrayList<String> experiments = new ArrayList<String>(); 
		
		if (!exact) {
			experiments.add("agreement_");
			return experiments;
		}
		
		experiments.add("agreement_agreementTo");
		experiments.add("agreement_agreementFrom");
		experiments.add("agreement_disagreementTo");
		experiments.add("agreement_disagreementFrom");
		experiments.add("agreement_noneTo");
		experiments.add("agreement_noneFrom");
		experiments.add("agreement_firstToAgreement");
		experiments.add("agreement_firstFromAgreement");
		experiments.add("agreement_firstToDisagreement");
		experiments.add("agreement_firstFromAgreement");
		experiments.add("agreement_firstToNone");
		experiments.add("agreement_firstFromNone");
		return experiments;
	}
	
	public static void buildAttributes(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges, ArrayList<String> featureNames) {
	
		_agreementTo = new Attribute("agreement_agreementTo");
		_agreementFrom = new Attribute("agreement_agreementFrom");
		_noneTo = new Attribute("agreement_noneTo");
		_noneFrom = new Attribute("agreement_noneFrom");
		_disagreementTo = new Attribute("agreement_disagreementTo");
		_disagreementFrom = new Attribute("agreement_disagreementFrom");
		_firstToAgreement = new Attribute("agreement_firstToAgreement");
		_firstFromAgreement = new Attribute("agreement_firstFromAgreement");
		_firstToDisagreement = new Attribute("agreement_firstToDisagreement");
		_firstFromDisagreement = new Attribute("agreement_firstFromDisagreement");
		_firstToNone = new Attribute("agreement_firstToNone");
		_firstFromNone = new Attribute("agreement_firstFromNone");
		
		addAtt(attributes, featureRanges, featureNames, _agreementTo, "agreement_agreementTo");
		addAtt(attributes, featureRanges, featureNames, _agreementFrom, "agreement_agreementFrom");
		addAtt(attributes, featureRanges, featureNames, _disagreementTo, "agreement_disagreementTo");
		addAtt(attributes, featureRanges, featureNames, _disagreementFrom, "agreement_disagreementFrom");
		addAtt(attributes, featureRanges, featureNames, _noneTo, _noneTo.name());
		addAtt(attributes, featureRanges, featureNames, _noneFrom, _noneFrom.name());
		addAtt(attributes, featureRanges, featureNames, _firstToAgreement, "agreement_firstTogreement");
		addAtt(attributes, featureRanges, featureNames, _firstFromAgreement, "agreement_firstFromgreement");
		addAtt(attributes, featureRanges, featureNames, _firstToDisagreement, "agreement_firstToDisagreement");
		addAtt(attributes, featureRanges, featureNames, _firstFromDisagreement, "agreement_firstFromDisagreement");
		addAtt(attributes, featureRanges, featureNames, _firstToNone, _firstToNone.name());
		addAtt(attributes, featureRanges, featureNames, _firstFromNone, _firstFromNone.name());
	}

	public static int firstFrom(Author author, Blog blog, HashMap<String,String> agreements, String type) {
		
		Object [] comments =  blog.getEntry(blog.getEntries().next()).getSortedComments();
		
		for (Object c : comments) {
			Comment comment = (Comment)c;
			if (comment.getUsername().equals(author.getName())) {
				String t = agreements.get(comment.getURL());
				if (t.equals(type)) return 1;
				else return 0;
			}
		}
		return 0;

	}
	
	public static void setInstance(Instance instance, HashMap<String,String> agreements, Author author, Blog blog, 
			double numPosts) {
		double agreementCount = 0;
		double disagreementCount = 0;
		double noneCount = 0;
				
		for (String url : author.getPosts()) {
			if (agreements.get(url).equals("agreement")) agreementCount++;
			else if (agreements.get(url).equals("disagreement")) disagreementCount++;
			else if (agreements.get(url).equals("none")) noneCount++;
		}
		
		instance.setValue(_agreementFrom,agreementCount > 0 ? agreementCount/numPosts : 0);
		instance.setValue(_disagreementFrom,disagreementCount > 0 ? disagreementCount/numPosts : 0);
		instance.setValue(_noneFrom,noneCount > 0 ? noneCount/numPosts : 0);
		instance.setValue(_agreementTo,computeTo(author, blog, agreements, "agreement")/numPosts);
		instance.setValue(_disagreementTo,computeTo(author, blog, agreements, "disagreement")/numPosts);
		instance.setValue(_noneTo,computeTo(author, blog, agreements, "none")/numPosts);
		instance.setValue(_firstToAgreement,firstTo(author, blog, agreements,"agreement"));
		instance.setValue(_firstFromAgreement,firstFrom(author, blog, agreements,"agreement"));
		instance.setValue(_firstToDisagreement,firstTo(author, blog, agreements,"disagreement"));
		instance.setValue(_firstFromDisagreement,firstFrom(author, blog, agreements,"disagreement"));
		instance.setValue(_firstToNone,firstTo(author, blog, agreements,"none"));
		instance.setValue(_firstFromNone,firstFrom(author, blog, agreements,"none"));
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/Armenian_Genocide__Archive_19___Rewriting_parts_of_this_article.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Heavy_metal_music__Archive_6___Degree_of_synonymity.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Israel_Defense_Forces__Archive_2___Image_gallery_edits.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//List_of_topics_characterized_as_pseudoscience__Archive_13___Darwinism.xml";
		String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-lj-july2011/threads/140578.xml";
		file = "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/13123.xml";
		AgreementModule module = new AgreementModule();
		module.run(file,"livejournal");
		module.read("/proj/nlp/users/sara/influence/processed/livejournal/processed-" + 
		new File(file).getName() + "/" + new File(file).getName()  + ".agreement");
	}

}
