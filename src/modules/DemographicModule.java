/**
 * 
 */
package modules;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import ob.util.Utils;
import processing.GeneralUtils;
import utils.DemographicFeatures;
import utils.DocumentDemographics;
import weka.core.Attribute;
import weka.core.Instance;

import edu.columbia.demographics.gender.*;
import edu.columbia.demographics.politics.PoliticsDetector;
import edu.columbia.demographics.religion.ReligionDetector;
import edu.columbia.demographics.age.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/**
 * @author sara
 *
 */
public class DemographicModule extends AbstractModule {

	String _modelDirectory;
	String _processingDirectory;
	GenderDetector _genderD;
	AgeDetector _ageD;
	AgeDetector _yearD;
	PoliticsDetector _politicalPartyD;
	PoliticsDetector _politicalViewD;
	ReligionDetector _religionD;
	

	public DemographicModule() {}
	
	/**
	 * 
	 */
	public DemographicModule(MaxentTagger tagger, LexicalizedParser parser, boolean processing) {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			_modelDirectory = prop.getProperty("demographic_model");
			_processingDirectory = prop.getProperty("processing");
			String xtract = prop.getProperty("xtract");
			
			if (processing) {
				_genderD = new GenderDetector(tagger,parser,xtract);
				_genderD.loadClassifier(_modelDirectory + "/svm-gender.model",_modelDirectory + "/gender.arff");
				
				_politicalViewD = new PoliticsDetector(tagger,"political_view",parser,xtract,false);
				_politicalViewD.loadClassifier(_modelDirectory + "/svm-political_view.model",_modelDirectory + "/political_view.arff");
	
				_politicalPartyD = new PoliticsDetector(tagger,"political_party",parser,xtract,false);
				_politicalPartyD.loadClassifier(_modelDirectory + "/svm-political_party.model",_modelDirectory + "/political_party.arff");
	
				_religionD = new ReligionDetector(tagger,"religion",parser,xtract,false);
				_religionD.loadClassifier(_modelDirectory + "/svm-religion.model",_modelDirectory + "/religion.arff");
				
				_ageD = new AgeDetector(tagger,parser,xtract,true);
				_ageD.loadClassifier(_modelDirectory + "/svm-age.model",_modelDirectory + "/year.arff");
				
				_yearD = new AgeDetector(tagger,parser,xtract,false);
				_yearD.loadClassifier(_modelDirectory + "/linear-year.model",_modelDirectory + "/year.arff");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/* (non-Javadoc)
	 * @see modules.AbstractModule#run(java.lang.String)
	 */
	@Override
	public void run(String file) {
		run(file,"");
	}
	
	public void run(String file, String type) {
		
		String fileName = new File(file).getName();
		
		String iFile = file;
		String oFile = _processingDirectory + "/" + type + "/processed-" + fileName + "/" + fileName;

		_genderD.run(iFile, oFile,"svm");
		_yearD.run(iFile, oFile,"linear");
		_ageD.run(iFile, oFile, "svm");
		_religionD.run(iFile,oFile,"svm");
		//_politicalViewD.run(iFile, oFile, "svm");
		_politicalPartyD.run(iFile, oFile, "svm");
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#read(java.lang.String)
	 */
	@Override
	public List<Object> read(String input) {
		return Arrays.asList(readByAuthor(input).getParticipants().values().toArray());
	}
	
	public DocumentDemographics readByAuthor(String input) {
		HashMap<String,Double []> demographics = new HashMap<String,Double []>();
		
		List<String> gender = Utils.readLines(input + ".gender");
		List<String> year = Utils.readLines(input + ".year");
		List<String> age = Utils.readLines(input + ".age");
		//List<String> politicalview = Utils.readLines(input + ".political_view");
		List<String> politicalparty = Utils.readLines(input + ".political_party");
		List<String> religion = Utils.readLines(input + ".religion");
		
		for (int index = 0; index < gender.size(); index++) {
			addDemographics(demographics,gender.get(index).split("\t"),0);
			addDemographics(demographics,year.get(index).split("\t"),1);
			addDemographics(demographics,age.get(index).split("\t"),2);
			addDemographics(demographics,politicalparty.get(index).split("\t"),3);
			//addDemographics(demographics,politicalview.get(index).split("\t"),4);
			addDemographics(demographics,religion.get(index).split("\t"),5);
		}
		return computeAverages(demographics);
		//return demographics;
	}
	
	private void addDemographics(HashMap<String,Double []> demographics, String [] d, int i) {
		if (demographics.containsKey(d[0])) {
			Double [] values = demographics.get(d[0]);
			values[i] = Double.valueOf(d[1]);
			demographics.put(d[0], values);
		}
		else {
			Double [] values = {Double.valueOf(d[1]),0.0,0.0,0.0,0.0,0.0};
			demographics.put(d[0], values);
		}
	}
	
	private DocumentDemographics computeAverages(HashMap<String,Double []> demographics) {
		
		DocumentDemographics d = new DocumentDemographics();
		
		for (String author : demographics.keySet()) {
			Double [] data = demographics.get(author);
			d.addParticipant(author, new DemographicFeatures(data));
		}
		return d;
	}
	
	private static Attribute _age, _ageBinary, _yearDistance, _majorityAge,
		_gender, /*_genderPercent,*/ _majorityGender,
		_religioni, _religionc, _religionj, _religiona, _majorityReligion,
		//_politicalView, _majorityPoliticalView,
		_politicalParty, _majorityPoliticalParty, 
		_maleYoung, _maleOld, _femaleYoung, _femaleOld,
		//_maleConservative, _maleLiberal, _femaleConservative, _femaleLiberal,
		_maleRepublican, _maleDemocrat, _femaleRepublican, _femaleDemocrat,
		//_youngLiberal, _youngConservative, _oldLiberal, _oldConservative, 
		_youngDemocrat, _youngRepublican, _oldDemocrat, _oldRepublican,
		//_conservativeRepublican, _conservativeDemocrat, _liberalRepublican, _liberalDemocrat, 
		
		_maleJewish, _maleMuslim, _femaleJewish, _femaleMuslim,
		_youngMuslim, _youngJewish, _oldMuslim, _oldJewish,
		//_conservativeJewish, _conservativeMuslim, _liberalJewish, _liberalMuslim,
		_republicanJewish, _republicanMuslim, _democratJewish, _democratMuslim,
		
		_maleAthiest, _maleChristian, _femaleAthiest, _femaleChristian,
		_youngChristian, _youngAthiest, _oldChristian, _oldAthiest,
		//_conservativeAthiest, _conservativeChristian, _liberalAthiest, _liberalChristian,
		_republicanAthiest, _democratChristian, _democratAthiest, _republicanChristian,
		_majorityAll, _majorityNone,
		_ageGenderMajority, //_agePoliticalViewMajority, _genderPoliticalViewMajority, 
		_agePoliticalPartyMajority, 
		_genderPoliticalPartyMajority, _ageReligionMajority, _genderReligionMajority,
		//_religionPoliticalViewMajority,
		_religionPoliticalPartyMajority; //, _politicalViewPartyMajority;
	
	public static int numExperiments(boolean combo, boolean majority, boolean binary) {
		int experiments = 0;
		
		if (binary) experiments += 9;
		if (majority) experiments += 6;
		if (combo && majority) experiments += 6 ;
		if (combo && binary) experiments += 36;
		return experiments;
	}
	
	public static ArrayList<String> getExperiments(boolean exact, boolean combo, boolean majority, boolean binary) {
		ArrayList<String> experiments = new ArrayList<String>(); 
		
		if (!exact) {
			experiments.add("demographic_");
			/*if (binary) experiments.add("demographic_binary_");
			if (majority) experiments.add("demographic_majority_");
			if (combo && binary) experiments.add("demographic_combo_binary");
			if (combo && majority) experiments.add("demographic_combo_majority");*/
			return experiments;
		}
		
		/*experiments.add("age");
		experiments.add("gender");
		experiments.add("religion");
		experiments.add("political_view");
		experiments.add("political_party");
		if (combo) {
			experiments.add("combo_");
			experiments.add("combomajority");
		}
		experiments.add("_majority");
		experiments.add("demographic_majority_");*/
		
		if (binary) {
			experiments.add("demographic_binary_age_exact");
			experiments.add("demographic_binary_age_binary");
			experiments.add("demographic_binary_age_distance");
			experiments.add("demographic_binary_gender");
			experiments.add("demographic_binary_religion");
			//experiments.add("demographic_binary_political_view");
			experiments.add("demographic_binary_political_party");
		}
		
		if (majority) {
			experiments.add("demographic_majority_age");
			experiments.add("demographic_majority_gender");
			experiments.add("demographic_majority_religion");
			//experiments.add("demographic_majority_political_view");
			experiments.add("demographic_majority_political_party");
			experiments.add("demographic_majority_all");
			experiments.add("demographic_majority_none");
		}

		
		if (combo) {
			if (binary || (combo && !(binary && majority))) {
	 			experiments.add("demographic_combo_binary_age_gender");
				//experiments.add("demographic_combo_binary_political_view_gender");
				//experiments.add("demographic_combo_binary_political_view_age");
				experiments.add("demographic_combo_binary_political_party_gender");
				experiments.add("demographic_combo_binary_political_party_age");
				//experiments.add("demographic_combo_binary_political_view_party");
				experiments.add("demographic_combo_binary_age_religion");
				experiments.add("demographic_combo_binary_gender_religion");
				experiments.add("demographic_combo_binary_religion_political_party");
				//experiments.add("demographic_combo_binary_religion_political_view");
			}
			if (majority || (combo && !(binary && majority))) {
				experiments.add("demographic_combo_majority_age_gender");
				//experiments.add("demographic_combo_majority_political_view_party");
				//experiments.add("demographic_combo_majority_political_view_age");
				//experiments.add("demographic_combo_majority_political_party_age");
				//experiments.add("demographic_combo_majority_political_view_gender");
				experiments.add("demographic_combo_majority_political_party_gender");
				experiments.add("demographic_combo_majority_age_religion");
				experiments.add("demographic_combo_majority_gender_religion");
				experiments.add("demographic_combo_majority_religion_political_party");
				//experiments.add("demographic_combo_majority_religion_political_view");
			}
		}
		
		//experiments.add("demographic_gender_percent");
		return experiments;
	}
	
	public static void buildAttributes(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges, ArrayList<String> featureNames, 
			boolean combo, boolean majority, boolean binary) {
	
		combo = true;
		binary = true;
		majority = true;
		
		// 9
		if (binary) {
			_age = new Attribute("demographic_binary_age_exact");
			_ageBinary = new Attribute("demographic_binary_age_binary");
			_yearDistance = new Attribute("demographic_binary_age_distance");	
			_gender = new Attribute("demographic_binary_gender");
			_religioni = new Attribute("demographic_binary_religion_i");
			_religionj = new Attribute("demographic_binary_religion_j");
			_religionc = new Attribute("demographic_binary_religion_c");
			_religiona = new Attribute("demographic_binary_religion_a");
			_politicalParty = new Attribute("demographic_binary_political_party");
			//_politicalView = new Attribute("demographic_binary_political_view");
			//_genderPercent = new Attribute("demographic_gender_percent");
			
			addAtt(attributes, featureRanges, featureNames, _age, "demographic_binary_age_exact");
			addAtt(attributes, featureRanges, featureNames, _ageBinary, "demographic_binary_age_binary");
			addAtt(attributes, featureRanges, featureNames, _yearDistance, "demographic_binary_age_distance");
			/////addAtt(attributes, featureRanges, featureNames, _genderPercent, "demographic_gender_percent");
			addAtt(attributes, featureRanges, featureNames, _gender, "demographic_binary_gender");
			addAtt(attributes, featureRanges, featureNames, _religioni, "demographic_binary_religion_i");
			addAtt(attributes, featureRanges, featureNames, _religiona, "demographic_binary_religion_a");
			addAtt(attributes, featureRanges, featureNames, _religionj, "demographic_binary_religion_j");
			addAtt(attributes, featureRanges, featureNames, _religionc, "demographic_binary_religion_c");
			//addAtt(attributes, featureRanges, featureNames, _politicalView, "demographic_binary_political_view");
			addAtt(attributes, featureRanges, featureNames, _politicalParty, "demographic_binary_political_party");
			
		}
		// 6
		if (majority) {
			_majorityAge = new Attribute("demographic_majority_age");
			_majorityGender = new Attribute("demographic_majority_gender");
			_majorityReligion = new Attribute("demographic_majority_religion");
			_majorityPoliticalParty = new Attribute("demographic_majority_political_party");
			//_majorityPoliticalView = new Attribute("demographic_majority_political_view");
			// 2
			_majorityAll = new Attribute("demographic_majority_all");
			_majorityNone = new Attribute("demographic_majority_none");

			addAtt(attributes, featureRanges, featureNames, _majorityAge, "demographic_majority_age");
			addAtt(attributes, featureRanges, featureNames, _majorityGender, "demographic_majority_gender");
			addAtt(attributes, featureRanges, featureNames, _majorityReligion, "demographic_majority_religion");
			//addAtt(attributes, featureRanges, featureNames, _majorityPoliticalView, "demographic_majority_political_view");
			addAtt(attributes, featureRanges, featureNames, _majorityPoliticalParty, "demographic_majority_political_party");
			addAtt(attributes, featureRanges, featureNames, _majorityAll, "demographic_majority_all");
			addAtt(attributes, featureRanges, featureNames, _majorityNone, "demographic_majority_none");
			
		}
		if (combo) {
			// 36
			if (binary) {
				// age gender
				_maleYoung = new Attribute("demographic_combo_binary_age_gender_maleYoung");
				_maleOld = new Attribute("demographic_combo_binary_age_gender_maleOld");
				_femaleYoung = new Attribute("demographic_combo_binary_age_gender_femaleYoung");
				_femaleOld = new Attribute("demographic_combo_binary_age_gender_femaleOld");
				addAtt(attributes, featureRanges, featureNames, _maleYoung, _maleYoung.name());
				addAtt(attributes, featureRanges, featureNames, _maleOld, _maleOld.name());
				addAtt(attributes, featureRanges, featureNames, _femaleYoung, _femaleYoung.name());
				addAtt(attributes, featureRanges, featureNames, _femaleOld, _femaleOld.name());
				
				// politics
				/*_conservativeRepublican = new Attribute("demographic_combo_binary_political_view_party_conservativeRepublican");
				_conservativeDemocrat = new Attribute("demographic_combo_binary_political_view_party_conservativeDemocrat");
				_liberalRepublican = new Attribute("demographic_combo_binary_political_view_party_liberalRepublican");
				_liberalDemocrat = new Attribute("demographic_combo_binary_political_view_party_liberalDemocrat");
				addAtt(attributes, featureRanges, featureNames, _liberalRepublican, _liberalRepublican.name());
				addAtt(attributes, featureRanges, featureNames, _liberalDemocrat, _liberalDemocrat.name());
				addAtt(attributes, featureRanges, featureNames, _conservativeRepublican, _conservativeRepublican.name());
				addAtt(attributes, featureRanges, featureNames, _conservativeDemocrat, _conservativeDemocrat.name());*/

				// age view
				/*_youngConservative = new Attribute("demographic_combo_binary_political_view_age_youngConservative");
				_youngLiberal = new Attribute("demographic_combo_binary_political_view_age_youngLiberal");
				_oldConservative = new Attribute("demographic_combo_binary_political_view_age_oldConservative");
				_oldLiberal = new Attribute("demographic_combo_binary_political_view_age_oldLiberal");
				addAtt(attributes, featureRanges, featureNames, _youngConservative, _youngConservative.name());
				addAtt(attributes, featureRanges, featureNames, _youngLiberal, _youngLiberal.name());
				addAtt(attributes, featureRanges, featureNames, _oldConservative, _oldConservative.name());
				addAtt(attributes, featureRanges, featureNames, _oldLiberal, _oldLiberal.name());*/
				
				// age party
				_youngDemocrat = new Attribute("demographic_combo_binary_political_party_age_youngDemocrat");
				_youngRepublican = new Attribute("demographic_combo_binary_political_party_age_youngRepublican");
				_oldDemocrat = new Attribute("demographic_combo_binary_political_party_age_oldDemocrat");
				_oldRepublican = new Attribute("demographic_combo_binary_political_party_age_oldRepublican");
				addAtt(attributes, featureRanges, featureNames, _youngDemocrat, _youngDemocrat.name());
				addAtt(attributes, featureRanges, featureNames, _youngRepublican, _youngRepublican.name());
				addAtt(attributes, featureRanges, featureNames, _oldDemocrat, _oldDemocrat.name());
				addAtt(attributes, featureRanges, featureNames, _oldRepublican, _oldRepublican.name());
				
				// gender view
				/*_maleConservative = new Attribute("demographic_combo_binary_political_view_gender_maleConservative");
				_maleLiberal = new Attribute("demographic_combo_binary_political_view_gender_maleLiberal");
				_femaleConservative = new Attribute("demographic_combo_binary_political_view_gender_femaleConservative");
				_femaleLiberal = new Attribute("demographic_combo_binary_political_view_gender_femaleLiberal");
				addAtt(attributes, featureRanges, featureNames, _maleConservative, _maleConservative.name());
				addAtt(attributes, featureRanges, featureNames, _maleLiberal, _maleLiberal.name());
				addAtt(attributes, featureRanges, featureNames, _femaleConservative, _femaleConservative.name());
				addAtt(attributes, featureRanges, featureNames, _femaleLiberal, _femaleLiberal.name());*/
				
				// gender party
				_maleRepublican = new Attribute("demographic_combo_binary_political_party_gender_maleRepublican");
				_maleDemocrat = new Attribute("demographic_combo_binary_political_party_gender_maleDemocrat");
				_femaleRepublican = new Attribute("demographic_combo_binary_political_party_gender_femaleRepublican");
				_femaleDemocrat = new Attribute("demographic_combo_binary_political_party_gender_femaleDemocrat");
				addAtt(attributes, featureRanges, featureNames, _maleDemocrat, _maleDemocrat.name());
				addAtt(attributes, featureRanges, featureNames, _maleRepublican, _maleRepublican.name());
				addAtt(attributes, featureRanges, featureNames, _femaleDemocrat, _femaleDemocrat.name());
				addAtt(attributes, featureRanges, featureNames, _femaleRepublican, _femaleRepublican.name());

				// gender religion
				_maleJewish = new Attribute("demographic_combo_binary_gender_religion_maleJewish");
				_maleChristian = new Attribute("demographic_combo_binary_gender_religion_maleChristian");
				_femaleJewish = new Attribute("demographic_combo_binary_gender_religion_femaleJewish");
				_femaleChristian = new Attribute("demographic_combo_binary_gender_religion_femaleChristian");
				_maleMuslim = new Attribute("demographic_combo_binary_gender_religion_maleMuslim");
				_maleAthiest = new Attribute("demographic_combo_binary_gender_religion_maleAthiest");
				_femaleMuslim = new Attribute("demographic_combo_binary_gender_religion_femaleMuslim");
				_femaleAthiest = new Attribute("demographic_combo_binary_gender_religion_femaleAthiest");
				addAtt(attributes, featureRanges, featureNames, _maleJewish, _maleJewish.name());
				addAtt(attributes, featureRanges, featureNames, _maleChristian, _maleChristian.name());
				addAtt(attributes, featureRanges, featureNames, _femaleJewish, _femaleJewish.name());
				addAtt(attributes, featureRanges, featureNames, _femaleChristian, _femaleChristian.name());
				addAtt(attributes, featureRanges, featureNames, _maleMuslim, _maleMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _maleAthiest, _maleAthiest.name());
				addAtt(attributes, featureRanges, featureNames, _femaleMuslim, _femaleMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _femaleAthiest, _femaleAthiest.name());
				
				// age religion
				_oldJewish = new Attribute("demographic_combo_binary_age_religion_oldJewish");
				_oldChristian = new Attribute("demographic_combo_binary_age_religion_oldChristian");
				_youngJewish = new Attribute("demographic_combo_binary_age_religion_youngJewish");
				_youngChristian = new Attribute("demographic_combo_binary_age_religion_youngChristian");
				_oldMuslim = new Attribute("demographic_combo_binary_age_religion_oldMuslim");
				_oldAthiest = new Attribute("demographic_combo_binary_age_religion_oldAthiest");
				_youngMuslim = new Attribute("demographic_combo_binary_age_religion_youngMuslim");
				_youngAthiest = new Attribute("demographic_combo_binary_age_religion_youngAthiest");
				addAtt(attributes, featureRanges, featureNames, _oldJewish, _oldJewish.name());
				addAtt(attributes, featureRanges, featureNames, _oldChristian, _oldChristian.name());
				addAtt(attributes, featureRanges, featureNames, _youngJewish, _youngJewish.name());
				addAtt(attributes, featureRanges, featureNames, _youngChristian, _youngChristian.name());
				addAtt(attributes, featureRanges, featureNames, _oldMuslim, _oldMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _oldAthiest, _oldAthiest.name());
				addAtt(attributes, featureRanges, featureNames, _youngMuslim, _youngMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _youngAthiest, _youngAthiest.name());
				
				// view religion
				/*_liberalJewish = new Attribute("demographic_combo_binary_religion_political_view_liberalJewish");
				_liberalChristian = new Attribute("demographic_combo_binary_religion_political_view_liberalChristian");
				_conservativeJewish = new Attribute("demographic_combo_binary_religion_political_view_conservativeJewish");
				_conservativeChristian = new Attribute("demographic_combo_binary_religion_political_view_conservativeChristian");
				_liberalMuslim = new Attribute("demographic_combo_binary_religion_political_view_liberalMuslim");
				_liberalAthiest = new Attribute("demographic_combo_binary_religion_political_view_liberalAthiest");
				_conservativeMuslim = new Attribute("demographic_combo_binary_religion_political_view_conservativeMuslim");
				_conservativeAthiest = new Attribute("demographic_combo_binary_religion_political_view_conservativeAthiest");
				addAtt(attributes, featureRanges, featureNames, _liberalJewish, _liberalJewish.name());
				addAtt(attributes, featureRanges, featureNames, _liberalChristian, _liberalChristian.name());
				addAtt(attributes, featureRanges, featureNames, _conservativeJewish, _conservativeJewish.name());
				addAtt(attributes, featureRanges, featureNames, _conservativeChristian, _conservativeChristian.name());
				addAtt(attributes, featureRanges, featureNames, _liberalMuslim, _liberalMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _liberalAthiest, _liberalAthiest.name());
				addAtt(attributes, featureRanges, featureNames, _conservativeMuslim, _conservativeMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _conservativeAthiest, _conservativeAthiest.name());*/
				
				// party religion
				_republicanJewish = new Attribute("demographic_combo_binary_religion_political_party_republicanJewish");
				_republicanChristian = new Attribute("demographic_combo_binary_religion_political_party_republicanChristian");
				_democratJewish = new Attribute("demographic_combo_binary_religion_political_party_democratJewish");
				_democratChristian = new Attribute("demographic_combo_binary_religion_political_party_democratChristian");
				_republicanMuslim = new Attribute("demographic_combo_binary_religion_political_party_republicanMuslim");
				_republicanAthiest = new Attribute("demographic_combo_binary_religion_political_party_republicanAthiest");
				_democratMuslim = new Attribute("demographic_combo_binary_religion_political_party_democratMuslim");
				_democratAthiest = new Attribute("demographic_combo_binary_religion_political_party_democratAthiest");
				addAtt(attributes, featureRanges, featureNames, _democratJewish, _democratJewish.name());
				addAtt(attributes, featureRanges, featureNames, _democratChristian, _democratChristian.name());
				addAtt(attributes, featureRanges, featureNames, _republicanJewish, _republicanJewish.name());
				addAtt(attributes, featureRanges, featureNames, _republicanChristian, _republicanChristian.name());
				addAtt(attributes, featureRanges, featureNames, _democratMuslim, _democratMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _democratAthiest, _democratAthiest.name());
				addAtt(attributes, featureRanges, featureNames, _republicanMuslim, _republicanMuslim.name());
				addAtt(attributes, featureRanges, featureNames, _republicanAthiest, _republicanAthiest.name());
			}
			// 6
			if (majority) {
				_ageGenderMajority = new Attribute("demographic_combo_majority_age_gender");
				//_politicalViewPartyMajority = new Attribute("demographic_combo_majority_political_view_party");
				//_agePoliticalViewMajority = new Attribute("demographic_combo_majority_political_view_age");
				_agePoliticalPartyMajority = new Attribute("demographic_combo_majority_political_party_age");
				//_genderPoliticalViewMajority = new Attribute("demographic_combo_majority_political_view_gender");
				_genderPoliticalPartyMajority = new Attribute("demographic_combo_majority_political_party_gender");
				_ageReligionMajority = new Attribute("demographic_combo_majority_age_religion");
				_genderReligionMajority = new Attribute("demographic_combo_majority_gender_religion");
				_religionPoliticalPartyMajority = new Attribute("demographic_combo_majority_religion_political_party");
				//_religionPoliticalViewMajority = new Attribute("demographic_combo_majority_religion_political_view");

				addAtt(attributes, featureRanges, featureNames, _ageGenderMajority, _ageGenderMajority.name());
				//addAtt(attributes, featureRanges, featureNames, _politicalViewPartyMajority, _politicalViewPartyMajority.name());
				addAtt(attributes, featureRanges, featureNames, _agePoliticalPartyMajority, _agePoliticalPartyMajority.name());
				//addAtt(attributes, featureRanges, featureNames, _agePoliticalViewMajority, _agePoliticalViewMajority.name());
				addAtt(attributes, featureRanges, featureNames, _genderPoliticalPartyMajority, _genderPoliticalPartyMajority.name());
				//addAtt(attributes, featureRanges, featureNames, _genderPoliticalViewMajority, _genderPoliticalViewMajority.name());
				addAtt(attributes, featureRanges, featureNames, _ageReligionMajority, _ageReligionMajority.name());
				addAtt(attributes, featureRanges, featureNames, _genderReligionMajority, _genderReligionMajority.name());
				addAtt(attributes, featureRanges, featureNames, _religionPoliticalPartyMajority, _religionPoliticalPartyMajority.name());
				//addAtt(attributes, featureRanges, featureNames, _religionPoliticalViewMajority, _religionPoliticalViewMajority.name());	
			}
		}
	}
	
	public static void setInstance(Instance instance, DocumentDemographics demographics, String author, 
			boolean combo, boolean majority, boolean binary) {
		
		combo = true;
		binary = true;
		majority = true;
		
		DemographicFeatures participant = demographics.getParticipant(author);
		
		if (participant == null) {
			System.out.println(author);
		}
		
		if (binary) {
			instance.setValue(_age, participant == null ? 0 : participant.getYear());
			instance.setValue(_ageBinary, participant == null ? 0 : participant.getBinaryAge());
			instance.setValue(_yearDistance, participant == null ? 0 : demographics.getAverageYear() - participant.getYear());
			instance.setValue(_gender, participant == null ? 0 : participant.getGender()); 
			instance.setValue(_religionj, participant == null ? 0 : participant.getReligion() == 0 ? 1 : 0);
			instance.setValue(_religioni, participant == null ? 0 : participant.getReligion() == 1 ? 1 : 0);
			instance.setValue(_religionc, participant == null ? 0 : participant.getReligion() == 2 ? 1 : 0);
			instance.setValue(_religiona, participant == null ? 0 : participant.getReligion() == 3 ? 1 : 0);
			//instance.setValue(_politicalView, participant == null ? 0 : participant.getPoliticalView()); 
			instance.setValue(_politicalParty, participant == null ? 0 : participant.getPoliticalParty()); 
		}
		
		if (majority) {
			int m = demographics.getMajority(demographics.getAge(),participant.getBinaryAge());
			double total = m;
			//if (m == 1 && participant.getBinaryAge() == 1) System.out.println("Majority Young");
			//else System.out.println("Majority Old");
			instance.setValue(_majorityAge, m);
			m = demographics.getMajority(demographics.getGender(),participant.getGender());
			//if (m == 1 && participant.getGender() == 1) System.out.println("Majority Female");
			//else System.out.println("Majority Old");
			total += m;
			instance.setValue(_majorityGender, m); 
			m = demographics.getMajority(demographics.getReligion(),participant.getReligion());
			total += m;
			instance.setValue(_majorityReligion, m); 
			/*if (m == 1 && participant.getReligion() == 0) System.out.println("Majority Jewish");
			if (m == 1 && participant.getReligion() == 1) System.out.println("Majority Islam");
			if (m == 1 && participant.getReligion() == 2) System.out.println("Majority Christian");
			if (m == 1 && participant.getReligion() == 3) System.out.println("Majority Atheist");*/
			//m = participant == null ? 0 : demographics.getMajority(demographics.getPoliticalView(),participant.getPoliticalView());
			//total += m;
			//instance.setValue(_majorityPoliticalView, m); 
			m = participant == null ? 0 : demographics.getMajority(demographics.getPoliticalParty(),participant.getPoliticalParty());
			//if (m == 1 && participant.getPoliticalParty() == 1) System.out.println("Majority Democrat");
			//else System.out.println("Majority Republican");
			total += m;
			instance.setValue(_majorityPoliticalParty, m); 
			
			instance.setValue(_majorityAll, total/4 == 1 ? 1 : 0);
			
			//if (total/4 == 1) {
			//	System.err.print("MAJORITY! " + author );
			//}
			
			instance.setValue(_majorityNone, total/4 == 0 ? 1 : 0);
		}
		
		if (combo) {
			// 56
			if (binary) {
				//instance.setValue(_politicalViewParty, participant == null ? 0 : participant.getPolitics());
				// age gender
				instance.setValue(_maleOld, participant == null ? 0 : participant.isMale() && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_maleYoung, participant == null ? 0 : participant.isMale() && participant.getBinaryAge() == 1 ? 1 : 0);
				instance.setValue(_femaleOld, participant == null ? 0 : !participant.isMale() && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_femaleYoung, participant == null ? 0 : !participant.isMale() && participant.getBinaryAge() == 1 ? 1 : 0);
				// party view
				/*instance.setValue(_conservativeDemocrat, participant == null ? 0 : participant.getPoliticalParty() == 1 && participant.getPoliticalView() == 0 ? 1 : 0);
				instance.setValue(_conservativeRepublican, participant == null ? 0 : participant.getPoliticalParty() == 0 && participant.getPoliticalView() == 0 ? 1 : 0);
				instance.setValue(_liberalDemocrat, participant == null ? 0 : participant.getPoliticalParty() == 1 && participant.getPoliticalView() == 1 ? 1 : 0);
				instance.setValue(_liberalRepublican, participant == null ? 0 : participant.getPoliticalParty() == 0 && participant.getPoliticalView() == 1 ? 1 : 0);
				// age politic view
				instance.setValue(_oldLiberal, participant == null ? 0 : participant.getPoliticalView() == 1 && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_oldConservative, participant == null ? 0 : participant.getPoliticalView() == 0 && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_youngLiberal, participant == null ? 0 : participant.getPoliticalView() == 1 && participant.getBinaryAge() == 1 ? 1 : 0);
				instance.setValue(_youngConservative, participant == null ? 0 : participant.getPoliticalView() == 0 && participant.getBinaryAge() == 1 ? 1 : 0);
				*/// age politic party
				instance.setValue(_oldDemocrat, participant == null ? 0 : participant.getPoliticalParty() == 1 && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_oldRepublican, participant == null ? 0 : participant.getPoliticalParty() == 0 && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_youngDemocrat, participant == null ? 0 : participant.getPoliticalParty() == 1 && participant.getBinaryAge() == 1 ? 1 : 0);
				instance.setValue(_youngRepublican, participant == null ? 0 : participant.getPoliticalParty() == 0 && participant.getBinaryAge() == 1 ? 1 : 0);
				// gender politic view
				/*instance.setValue(_maleLiberal, participant == null ? 0 : participant.getPoliticalView() == 1 && participant.isMale() ? 1 : 0);
				instance.setValue(_maleConservative, participant == null ? 0 : participant.getPoliticalView() == 0 && participant.isMale() ? 1 : 0);
				instance.setValue(_femaleLiberal, participant == null ? 0 : participant.getPoliticalView() == 1 && !participant.isMale() ? 1 : 0);
				instance.setValue(_femaleConservative, participant == null ? 0 : participant.getPoliticalView() == 0 && !participant.isMale() ? 1 : 0);
				*/// gender politic party
				instance.setValue(_maleDemocrat, participant == null ? 0 : participant.getPoliticalParty() == 1 && participant.isMale() ? 1 : 0);
				instance.setValue(_maleRepublican, participant == null ? 0 : participant.getPoliticalParty() == 0 && participant.isMale() ? 1 : 0);
				instance.setValue(_femaleDemocrat, participant == null ? 0 : participant.getPoliticalParty() == 1 && !participant.isMale() ? 1 : 0);
				instance.setValue(_femaleRepublican, participant == null ? 0 : participant.getPoliticalParty() == 0 && !participant.isMale() ? 1 : 0);
				// age religion
				instance.setValue(_oldChristian, participant == null ? 0 : participant.getReligion() == 2 && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_oldMuslim, participant == null ? 0 : participant.getReligion() == 1 && participant.getBinaryAge() == 0 ? 1 : 0);
				instance.setValue(_youngChristian, participant == null ? 0 : participant.getReligion() == 2 && participant.getBinaryAge() == 1? 1 : 0);
				instance.setValue(_youngMuslim, participant == null ? 0 : participant.getReligion() == 1 && participant.getBinaryAge() == 1? 1 : 0);
				instance.setValue(_oldJewish, participant == null ? 0 : participant.getReligion() == 0 && participant.getBinaryAge() == 0? 1 : 0);
				instance.setValue(_oldAthiest, participant == null ? 0 : participant.getReligion() == 3 && participant.getBinaryAge() == 0? 1 : 0);
				instance.setValue(_youngJewish, participant == null ? 0 : participant.getReligion() == 0 && participant.getBinaryAge() == 1? 1 : 0);
				instance.setValue(_youngAthiest, participant == null ? 0 : participant.getReligion() == 3 && participant.getBinaryAge() == 1? 1 : 0);
				// gender religion
				instance.setValue(_maleChristian, participant == null ? 0 : participant.getReligion() == 2 && participant.isMale()? 1 : 0);
				instance.setValue(_maleMuslim, participant == null ? 0 : participant.getReligion() == 1 && participant.isMale()? 1 : 0);
				instance.setValue(_femaleChristian, participant == null ? 0 : participant.getReligion() == 2 && !participant.isMale()? 1 : 0);
				instance.setValue(_femaleMuslim, participant == null ? 0 : participant.getReligion() == 1 && !participant.isMale()? 1 : 0);
				instance.setValue(_maleJewish, participant == null ? 0 : participant.getReligion() == 0 && participant.isMale()? 1 : 0);
				instance.setValue(_maleAthiest, participant == null ? 0 : participant.getReligion() == 3 && participant.isMale()? 1 : 0);
				instance.setValue(_femaleJewish, participant == null ? 0 : participant.getReligion() == 0 && !participant.isMale()? 1 : 0);
				instance.setValue(_femaleAthiest, participant == null ? 0 : participant.getReligion() == 3 && !participant.isMale()? 1 : 0);
				/*// political view religion
				instance.setValue(_conservativeChristian, participant == null ? 0 : participant.getReligion() == 2 && participant.getPoliticalView() == 0? 1 : 0);
				instance.setValue(_conservativeMuslim, participant == null ? 0 : participant.getReligion() == 1 && participant.getPoliticalView() == 0? 1 : 0);
				instance.setValue(_liberalChristian, participant == null ? 0 : participant.getReligion() == 2 && participant.getPoliticalView() == 1? 1 : 0);
				instance.setValue(_liberalMuslim, participant == null ? 0 : participant.getReligion() == 1 && participant.getPoliticalView() == 1? 1 : 0);
				instance.setValue(_conservativeJewish, participant == null ? 0 : participant.getReligion() == 0 && participant.getPoliticalView() == 0? 1 : 0);
				instance.setValue(_conservativeAthiest, participant == null ? 0 : participant.getReligion() == 3 && participant.getPoliticalView() == 0? 1 : 0);
				instance.setValue(_liberalJewish, participant == null ? 0 : participant.getReligion() == 0 && participant.getPoliticalView() == 1? 1 : 0);
				instance.setValue(_liberalAthiest, participant == null ? 0 : participant.getReligion() == 3 && participant.getPoliticalView() == 1? 1 : 0);
				*/// political party religion
				instance.setValue(_republicanChristian, participant == null ? 0 : participant.getReligion() == 2 && participant.getPoliticalParty() == 0? 1 : 0);
				instance.setValue(_republicanMuslim, participant == null ? 0 : participant.getReligion() == 1 && participant.getPoliticalParty() == 0? 1 : 0);
				instance.setValue(_democratChristian, participant == null ? 0 : participant.getReligion() == 2 && participant.getPoliticalParty() == 1? 1 : 0);
				instance.setValue(_democratMuslim, participant == null ? 0 : participant.getReligion() == 1 && participant.getPoliticalParty() == 1? 1 : 0);
				instance.setValue(_republicanJewish, participant == null ? 0 : participant.getReligion() == 0 && participant.getPoliticalParty() == 0? 1 : 0);
				instance.setValue(_republicanAthiest, participant == null ? 0 : participant.getReligion() == 3 && participant.getPoliticalParty() == 0? 1 : 0);
				instance.setValue(_democratJewish, participant == null ? 0 : participant.getReligion() == 0 && participant.getPoliticalParty() == 1? 1 : 0);
				instance.setValue(_democratAthiest, participant == null ? 0 : participant.getReligion() == 3 && participant.getPoliticalParty() == 1? 1 : 0);
			}
			
			if (majority) {
				/*instance.setValue(_politicalViewPartyMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getPoliticalParty(), participant.getPoliticalParty(), 
							demographics.getPoliticalView(), participant.getPoliticalView()));*/
				instance.setValue(_ageGenderMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getAge(), participant.getBinaryAge(), 
							demographics.getGender(), participant.getGender()));
				/*instance.setValue(_agePoliticalViewMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getAge(), participant.getBinaryAge(), 
							demographics.getPoliticalView(), participant.getPoliticalView()));*/
				instance.setValue(_agePoliticalPartyMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getPoliticalParty(), participant.getPoliticalParty(), 
							demographics.getAge(), participant.getBinaryAge()));
				/*instance.setValue(_genderPoliticalViewMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getGender(), participant.getGender(), 
							demographics.getPoliticalView(), participant.getPoliticalView()));*/
				instance.setValue(_genderPoliticalPartyMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getPoliticalParty(), participant.getPoliticalParty(), 
							demographics.getGender(), participant.getGender()));
				instance.setValue(_ageReligionMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getAge(), participant.getBinaryAge(), 
							demographics.getReligion(), participant.getReligion()));
				instance.setValue(_genderReligionMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getGender(), participant.getGender(), 
							demographics.getReligion(), participant.getReligion()));
				/*instance.setValue(_religionPoliticalViewMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getReligion(), participant.getReligion(), 
							demographics.getPoliticalView(), participant.getPoliticalView()));*/
				instance.setValue(_religionPoliticalPartyMajority, participant == null ? 0 : 
					demographics.getMajorityCombo(demographics.getPoliticalParty(), participant.getPoliticalParty(), 
							demographics.getReligion(), participant.getReligion()));
			}
		}
		//instance.setValue(_genderPercent, participant == null ? 0 : demographics.getGenderPercent(participant.isMale())); 

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/Armenian_Genocide__Archive_19___Rewriting_parts_of_this_article.xml";
		String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Heavy_metal_music__Archive_6___Degree_of_synonymity.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Israel_Defense_Forces__Archive_2___Image_gallery_edits.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//List_of_topics_characterized_as_pseudoscience__Archive_13___Darwinism.xml";
		file = "/proj/nlp/fluke/SCIL/data/influencer/wiki-10/threads/Lymphoma___WHO_Classification.xml";
		//"/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch1LJ2/Abortion__Archive_23___Activist_pictures.xml";
		file = "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/13123.xml";
		
		try {
			DemographicModule demographics = new DemographicModule(new MaxentTagger("edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger"),LexicalizedParser.loadModel(),true);
			demographics.run(file,"livejournal");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
