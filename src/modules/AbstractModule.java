/**
 * 
 */
package modules;

import java.util.*;

import blog.Sentence;
import blog.threaded.Comment;
import blog.threaded.livejournal.Blog;
import blog.threaded.livejournal.Entry;

import weka.core.Attribute;

/**
 * @author sara
 *
 */
public abstract class AbstractModule {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	/* every module should have the ability to be run, 
	 and for the output to be read and written.
	 */
	public abstract void run(String file);
	public abstract List<Object> read(String input);
	//public abstract void write(String output);
	//public abstract ArrayList<String> getExperiments(boolean exact);
	
	protected static void addAtt(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges,
			ArrayList<String> featureNames, Attribute a, String name) {
			attributes.add(a);
		   	featureRanges.add(1);
			featureNames.add(name);
	}
	
	protected static List<Sentence> getPost(Blog blog, String url) {
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		if (entry.getURL().equals(url) || url.equals("-1")) return entry.getSentences();
		return entry.getComment(url).getSentences();
	}
	
	/**
	 * Given a blog, generate a list of authors and their corresponding posts
	 * @param file
	 * @return
	 */
	protected HashMap<String,ArrayList<String>> getAuthorPostList(Entry entry, String blogger) {
		
		HashMap<String, ArrayList<String>> authors = new HashMap<String, ArrayList<String>>();
		
		ArrayList<String> posts = new ArrayList<String>();
		posts.add("-1");
		authors.put(blogger,posts);
		
		for (String key : entry.getCommentKeys()) {
			Comment comment = (blog.threaded.Comment)entry.getComment(key);
			
			posts = authors.containsKey(comment.getUsername()) ? authors.get(comment.getUsername()) : new ArrayList<String>();
			posts.add(comment.getURL());
			authors.put(comment.getUsername(), posts);
		}
		return authors;
	}
	
	public static int computeNumSentences(Entry entry, List<String> posts) {
		
		int numSentences = 0;
		
		for (String id : posts) {
			List<Sentence> sentences = null;
			
			if (id.equals("-1")) sentences = entry.getSentences();
			else sentences = entry.getComment(id).getSentences();
			
			numSentences += sentences.size();
		}
		
		return numSentences;
	}
	
	public static String getText(Blog blog, ArrayList<String> posts) {
		
		String text = "";
		
		for (String url : posts) {
			Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
			if (entry.getURL().equals(url) || url.equals("-1")) text += " " + entry.getEntryText();
			else text += " " + entry.getComment(url).getCommentText();
		}
		return text.trim();
	}
	
}
