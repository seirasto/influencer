/**
 * 
 */
package modules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import utils.CredibilityFeatures;
import weka.core.Attribute;
import weka.core.Instance;
import blog.threaded.livejournal.Blog;
import blog.threaded.livejournal.Entry;

/**
 * @author sara
 *
 */
public class CredibilityModule extends AbstractModule {
	/**
	 * 
	 */
	public CredibilityModule() {
	}

	/* (non-Javadoc)
	 * @see modules.AbstractModule#run(java.lang.String)
	 */
	@Override
	public void run(String file) {
	}
	

	/* (non-Javadoc)
	 * @see modules.AbstractModule#read(java.lang.String)
	 */
	@Override
	public List<Object> read(String input) {
		// TODO Auto-generated method stub
		return Arrays.asList(readByAuthor(input).values().toArray());
	}
	
	public HashMap<String,CredibilityFeatures> readByAuthor(String file) {
		
		HashMap<String,CredibilityFeatures> cfs = new HashMap<String,CredibilityFeatures>();
		
		Blog blog = Blog.processBlog(file);
		Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		
		HashMap<String,ArrayList<String>> authors = getAuthorPostList(entry, blog.getUsername());
		
		HashSet<String> questionauthor = new HashSet<String>();
		int mostquestions = 0;
		
		for (String author : authors.keySet()) {
			
			ArrayList<String> posts = authors.get(author);
			int c = CredibilityFeatures.computeInquisitveness(entry, posts);
			if (c > mostquestions) {
				mostquestions = CredibilityFeatures.computeInquisitveness(entry, posts);
				questionauthor = new HashSet<String>();
				questionauthor.add(author);
			}
			else if (c == mostquestions) {
				questionauthor.add(author);
			}
			
			CredibilityFeatures cf = new CredibilityFeatures(blog, author, authors.keySet(), posts);
			
			cfs.put(author, cf);
		}
		for (Object author : questionauthor.toArray()) {
			cfs.get(author).setMostQuestions();
		}
		return cfs;
	}
	
	private static Attribute _links, _quotes, _honorifics, _mentions, _mentioned, _misspellings, _numbers,
	 _mostquestions, _inquisitiveness, /*_questionsFrequency,*/ _percentQuestions;
	
	public static ArrayList<String> getExperiments(boolean exact) { 
		ArrayList<String> experiments = new ArrayList<String>(); 
		
		if (!exact) {
			experiments.add("credibility_");
			return experiments;
		}
		
		experiments.add("credibility_links");
		experiments.add("credibility_quotes");
		experiments.add("credibility_honorifics");
		experiments.add("credibility_mentions");
		experiments.add("credibility_mentioned");
		experiments.add("credibility_misspellings");
		experiments.add("credibility_numbers");
		experiments.add("credibility_mostquestions");
		experiments.add("credibility_inquisitiveness");
		experiments.add("credibility_percentquestions");
		//experiments.add("credibility_questionsfrequency");

		return experiments;
	}
	
	public static void buildAttributes(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges, ArrayList<String> featureNames) {
	
		_links = new Attribute("credibility_links");
		_quotes = new Attribute("credibility_quotes");
		_honorifics = new Attribute("credibility_honorifics");
		_mentions = new Attribute("credibility_mentions");
		_mentioned = new Attribute("credibility_mentioned");
		_misspellings = new Attribute("credibility_misspellings");
		_numbers = new Attribute("credibility_numbers");
		_inquisitiveness = new Attribute("credibility_inquisitiveness");
		_mostquestions = new Attribute("credibility_mostquestions");
		_percentQuestions = new Attribute("credibility_percentquestions");
		//_questionsFrequency = new Attribute("credibility_questionsfrequency");

		
		addAtt(attributes, featureRanges, featureNames, _links, "credibility_links");
		addAtt(attributes, featureRanges, featureNames, _quotes, "credibility_quotes");
		addAtt(attributes, featureRanges, featureNames, _honorifics, "credibility_honorifics");
		addAtt(attributes, featureRanges, featureNames, _mentions, "credibility_mentions");
		addAtt(attributes, featureRanges, featureNames, _mentioned, "credibility_mentioned");
		addAtt(attributes, featureRanges, featureNames, _misspellings, "credibility_misspellings");
		addAtt(attributes, featureRanges, featureNames, _numbers, "credibility_numbers");
		addAtt(attributes, featureRanges, featureNames, _mostquestions, "credibility_mostquestions");
		addAtt(attributes, featureRanges, featureNames, _inquisitiveness, "credibility_inquisitiveness");
		addAtt(attributes, featureRanges, featureNames, _percentQuestions, "credibility_percentquestions");
		//addAtt(attributes, featureRanges, featureNames, _questionsFrequency, "credibility_questionsfrequency");


	}

	public static void setInstance(Instance instance, CredibilityFeatures cf) {
		instance.setValue(_links, cf.getLinks()/(double)cf.numPosts()); 
		instance.setValue(_quotes, cf.getQuotes()/(double)cf.numPosts());
		instance.setValue(_honorifics, cf.getHonorifics()/(double)cf.numPosts());
		instance.setValue(_mentions, cf.getMentions()/(double)cf.numPosts());
		instance.setValue(_mentioned, cf.getMentioned()/(double)cf.numPosts());
		instance.setValue(_misspellings, cf.getMisspellings()/(double)cf.numPosts());
		instance.setValue(_numbers, cf.getNumbers()/(double)cf.numPosts());
		instance.setValue(_mostquestions, cf.hasMostQuestions() ? 1 : 0);
		instance.setValue(_inquisitiveness, cf.inquisitiveness() > 0 ? 1 : 0); 
		instance.setValue(_percentQuestions,cf.postInquisitiveness() == 0 ? 0 : cf.postInquisitiveness()/ (double)cf.numPosts());
		//instance.setValue(_questionsFrequency,cf.getActiveTime() == 0 ? 0 : df.inquisitiveness()/df.getActiveTime());

	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/Armenian_Genocide__Archive_19___Rewriting_parts_of_this_article.xml";
		//String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch1LJ2/Santa_Claus__Archive_7___Many_Thanks.xml";
		String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch1LJ2/Barack_Obama__Archive_70___Comma.xml";
		CredibilityModule module = new CredibilityModule();
		HashMap<String,CredibilityFeatures> credibilityFeatures = module.readByAuthor(file);
		
		for (String key : credibilityFeatures.keySet()) {
			System.out.println(key + ". " + credibilityFeatures.get(key));
		}
	}

}
