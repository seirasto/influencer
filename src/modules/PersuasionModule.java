/**
 * 
 */
package modules;

import java.io.*;
import java.util.*;

import processing.Contractions;
import processing.GeneralUtils;
import utils.Author;
import weka.core.Attribute;
import weka.core.Instance;
import blog.Sentence;
import blog.threaded.livejournal.*;

/**
 * @author sara
 *
 */
public class PersuasionModule extends AbstractModule {

	boolean _overwrite = false;
	String _outputDirectory = null;
	Contractions _contractions = null;
	ClaimModule _claimModule = null;
	ArgumentationModule _argumentationModule = null;
	
	/**
	 * 
	 */
	public PersuasionModule(boolean overwrite, ClaimModule c, ArgumentationModule a, boolean processing) {
		_overwrite = overwrite;
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			_outputDirectory =  prop.getProperty("output");
			_contractions = new Contractions(prop.getProperty("contractions_directory") + "/contractions-parserversion.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		_claimModule = c == null ? new ClaimModule(processing) : c;
		_argumentationModule = a == null ? new ArgumentationModule() : a;
	}
	
	/**
	 * 
	 */
	public PersuasionModule() {
		this(false,null,null,false);
	}


	/* (non-Javadoc)
	 * @see modules.AbstractModule#run(java.lang.String)
	 */
	@Override
	public void run(String file) {
		run(file,"");
	}
	
	public void run(String file, String type) {
		
		String processedFile = _outputDirectory + "/processed/" + type + "/processed-" + 
				new File(file).getName() + "/" + new File(file).getName();
		
		// TODO Auto-generated method stub
		if (_overwrite || !new File(processedFile + ".claims").exists()) {

			_claimModule.run(file);
		}		
		if (_overwrite || !new File(processedFile + ".argument").exists()) {
			_argumentationModule.run(file);
		}
	}
	/* (non-Javadoc)
	 * @see modules.AbstractModule#read(java.lang.String)
	 */
	@Override
	public List<Object> read(String input) {
		HashMap<String,ArrayList<Object>> posts = readByPost(input);
		ArrayList<Object> persuasion = new ArrayList<Object>();
		
		for (String key : posts.keySet()) {
			persuasion.addAll(posts.get(key));
		}
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see modules.AbstractModule#readByPost(java.lang.String)
	 */
	public HashMap<String,ArrayList<Object>> readByPost(String input) {
		HashMap<String,ArrayList<Object>> claims = _claimModule.readByPost(input + ".claims");
		HashMap<String,ArrayList<Object>> argumentations = _argumentationModule.readByPost(input + ".argument");
		HashMap<String,ArrayList<Object>> persuasion = new HashMap<String,ArrayList<Object>>();
		
		if (argumentations == null) return persuasion;
		
		Blog blog = Blog.processBlog(input + ".sentences");
		blog.threaded.livejournal.Entry entry = (blog.threaded.livejournal.Entry)blog.getEntry(blog.getEntries().next());
		persuasion.put("-1",getClaims(entry.getEntryText(), claims.get(entry.getURL()), argumentations.get(entry.getURL())));
		
		Iterator<String> comments = entry.getComments();
		
		while (comments.hasNext()) {
		// check for argumentation followed by a claim in the post
		//for (int index = 1; index < claims.size(); index++) {
			String url = comments.next();
			persuasion.put(url,getClaims(entry.getComment(url).getCommentText(), 
					claims.get(url), argumentations.get(url)));
		}
		return persuasion;
	}
	
	public ArrayList<Object> getClaims(String text, ArrayList<Object> claim, ArrayList<Object> argumentation) {
		ArrayList<Object> persuasions = new ArrayList<Object>();
		
		for (int i = 0; argumentation != null && i < argumentation.size(); i++) {
			int positionI = looseIndex(text,(String)argumentation.get(i));
			if (positionI == -1) {
				System.err.println("Argumentation not found: " + argumentation.get(i));
				System.err.println("Sentence: " + text);
				continue;
			}
			int minDistance = 1000;
			int claimIndex = -1; 
			int persuasionIndex = -1;
			boolean isClaim = false; // to indicate whether argumentation is also a claim
			
			for (int j = 0; j < claim.size(); j++) {
				
				int positionJ = looseIndex(text,(String)claim.get(j));
				if (positionJ == -1) {
					System.err.println("Claim not found: " + claim.get(j));
					continue;
				}
				
				if (claimIndex >= 0 && positionJ > positionI) break;

				int distance = Math.abs(positionJ - positionI);
				if (distance == 0) {
					isClaim = true;
					persuasionIndex = j;
				}
				
				// claim and justification can't be on same sentence...
				if (distance < minDistance && distance != 0) {
					minDistance = distance;
					claimIndex = j;
				}
			}
			
			if (claimIndex >= 0) persuasions.add(new Persuasion(claim.get(claimIndex),argumentation.get(i)));
			// if claim not followed by justification, but claim is a justification (i.e. same sentence), use as persuasion
			else if (isClaim) persuasions.add(new Persuasion(claim.get(persuasionIndex),argumentation.get(i)));
		}
		return persuasions;
	}
	
	private int looseIndex(String text, String substring) {
		try {
			
			text = _contractions.swapContractions(text.toLowerCase());
			text = text.replaceAll("-[lr][rc]b-","").replaceAll("\\p{Punct}+","").replaceAll("\\s+","");
			text = text.replaceAll("aaanchorrr", "lllinkkk");
			substring = _contractions.swapContractions(substring.toLowerCase());
			substring = substring.replaceAll("-[lr][rc]b-","").replaceAll("\\p{Punct}+", "").replaceAll("\\s+","").toLowerCase();
			substring = substring.replaceAll("aaanchorrr", "lllinkkk");
			
			
			
			//substring = substring.toLowerCase();
			//if (text.indexOf(substring.replaceAll("-[lr][rc]b-","").replaceAll("\\p{Punct}+", "").replaceAll("\\s+","")) >= 0)
			//	return text.indexOf(substring.replaceAll("-[lr][rc]b-","").replaceAll("\\p{Punct}+", "").replaceAll("\\s+",""));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (text.indexOf(substring) == -1) {
			//System.err.println("unequal!");
			if (substring.length() > 50)
				return text.indexOf(substring.substring(0,50));
		}
		
		return text.indexOf(substring);
	}
	
	public class Persuasion {
		String _claim;
		String _argumentation;
		
		public Persuasion(Object claim, Object argumentation) {
			_claim = (String)claim;
			_argumentation = (String)argumentation;
		}
		
		public String getClaim() {
			return _claim;
		}
		
		public String getPersuasion() {
			return _argumentation;
		}
		
		public String toString() {
			return"claim: " + _claim + ", argumentation: " + _argumentation;
		}
	}

	private static Attribute _persuasionHas, _persuasionCountPersuasion, _persuasionCountSentences, _numpersuasionPosts, _mostpersuasion;
	
	public static ArrayList<String> getExperiments(boolean exact) {
		ArrayList<String> experiments = new ArrayList<String>(); 
		
		if (!exact) {
			experiments.add("persuasion_");
			return experiments;
		}
		
		experiments.add("persuasion_has");
		experiments.add("persuasion_count_persuasion");
		experiments.add("persuasion_count_sentence");
		experiments.add("persuasion_numPosts");
		experiments.add("persuasion_most");
		return experiments;
	}
	
	public static void buildAttributes(ArrayList<Attribute> attributes, ArrayList<Integer> featureRanges, ArrayList<String> featureNames) {
	
		_persuasionHas = new Attribute("persuasion_has");
		_persuasionCountPersuasion = new Attribute("persuasion_count_persuasion");
		_persuasionCountSentences = new Attribute("persuasion_count_size");
		_numpersuasionPosts = new Attribute("persuasion_numPosts");
		_mostpersuasion = new Attribute("persuasion_most");
		addAtt(attributes, featureRanges, featureNames, _persuasionHas, "persuasion_has");
		addAtt(attributes, featureRanges, featureNames, _persuasionCountPersuasion, "persuasion_count_persuasion");
		addAtt(attributes, featureRanges, featureNames, _persuasionCountSentences, "persuasion_count_sentence");
		addAtt(attributes, featureRanges, featureNames, _numpersuasionPosts, "persuasion_numPosts");
		addAtt(attributes, featureRanges, featureNames, _mostpersuasion, "persuasion_most");
	}
	
	public static void setInstance(Instance instance, HashMap<String,ArrayList<Object>> persuasions, Author author, Blog blog, 
			double numPersuasions, double numPosts, double numSentences) {
		
		double persuasionCount = 0;
		int numPersuasionPosts = 0;
		double most = 0;
		
		if (persuasions != null) {
			for (String url : author.getPosts()) {
				
				if (persuasions.get(url) == null || persuasions.get(url).size() == 0) continue;
				
				List<Sentence> text = getPost(blog, url);
				persuasionCount +=  persuasions.get(url).size();
				numPersuasionPosts++;
				if (( persuasions.get(url).size()/(double)text.size()) > most) most =  persuasions.get(url).size()/(double)text.size();
			}
		}
		
		instance.setValue(_persuasionHas,persuasionCount > 0 ? 1 : 0);
		instance.setValue(_persuasionCountPersuasion, persuasionCount > 0 ? persuasionCount/numPersuasions : 0);
		instance.setValue(_persuasionCountSentences, persuasionCount > 0 ? persuasionCount/numSentences: 0);
		instance.setValue(_numpersuasionPosts,numPersuasionPosts/author.getPosts().size());
		instance.setValue(_mostpersuasion,most);
	}				

	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String file = "/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/Armenian_Genocide__Archive_19___Rewriting_parts_of_this_article.xml";
		String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//List_of_topics_characterized_as_pseudoscience__Archive_13___Darwinism.xml";
		//String file = "/proj/nlp/fluke/SCIL/data/influencer/sc-annotated-wd-july2011/threads//Israel_Defense_Forces__Archive_2___Image_gallery_edits.xml";
		file = "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/13123.xml";
		
		PersuasionModule module = new PersuasionModule();
		module.run(file);
		HashMap<String,ArrayList<Object>> persuasion = module.readByPost("/proj/nlp/users/sara/influence/processed/wikipedia/processed-" + 
		new File(file).getName() + "/" + new File(file).getName());
		
		for (String key : persuasion.keySet()) {
			System.out.println(key + ". " + Arrays.toString(persuasion.get(key).toArray()));
		}
	}

}
